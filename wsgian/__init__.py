"""wsgian, my personal wsgi micro-framework"""

__version__ = "1.9"
__author__ = "Gianfranco Messori <gmessori@gmail.com>"

from .wsgian import App, mtime_url, quickstart, LOG_LEVELS, url_for

from . import utils
from . import auth
from . import validate
from . import template
from .template import render_template
from .template import render_template_block

# counters require MySQLdb
mysql_lib = False
try:
    import MySQLdb

    mysql_lib = True
except ImportError as e:
    # print("[WARNING]: Unable to load MySQLdb")
    pass

if mysql_lib:
    from . import counters
