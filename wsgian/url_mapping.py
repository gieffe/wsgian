import re
import urllib.parse

var_regex = re.compile(
    r"""
    \{          # The exact character "{"
    (\w+)       # The variable name (restricted to a-z, 0-9, _)
    (?::([^}]+))? # The optional :regex part
    \}          # The exact character "}"
    """,
    re.VERBOSE,
)


def template_to_regex(template):
    regex = ""
    last_pos = 0
    for match in var_regex.finditer(template):
        regex += re.escape(template[last_pos : match.start()])
        var_name = match.group(1)
        expr = match.group(2) or "[^/]+"
        expr = "(?P<{}>{})".format(var_name, expr)
        regex += expr
        last_pos = match.end()
    regex += re.escape(template[last_pos:])
    regex = "^%s$" % regex
    return regex


class UrlFor:
    def __init__(self):
        self.routes = []
        self.config = {}

    def set_config(self, pard):
        self.config = pard

    def add_route(self, url):
        d = {}
        d["_pattern"] = url["pattern"]
        d["pattern"] = re.compile(template_to_regex(url["pattern"]))
        d["module"] = url["module"]
        if "action" in url:
            d["action"] = url["action"]
        if "file_path" in url:
            d["file_path"] = url["file_path"]
        d["login"] = url.get("login", False)
        self.routes.append(d)

    def get_controller(self, path):
        for rule in self.routes:
            regex = rule["pattern"]
            match = regex.match(path)
            if match:
                vars = match.groupdict()
                vars.update(rule)
                del vars["pattern"]
                return vars
        else:
            return {}

    def __call__(self, module, action, **args):
        for rule in self.routes:
            if module == rule["module"] and action == rule.get("action", "start"):
                url = var_regex.sub(r"{\1}", rule["_pattern"])
                url = url.format(**args)
                if args and "params" in args:
                    qs = "?" + urllib.parse.urlencode(args["params"], doseq=True)
                else:
                    qs = ""
                APPSERVER = self.config.get("APPSERVER", "")
                if APPSERVER and APPSERVER[-1] == "/":
                    APPSERVER = APPSERVER[:-1]
                return APPSERVER + url + qs
        else:
            raise ValueError("Path not found")


url_for = UrlFor()
