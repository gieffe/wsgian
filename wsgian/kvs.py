"""
KeyValueStorage is a class that makes it easy and convenient to
manage a table (mysql) of parameters of the type "name, key, value."

One can easily create subclasses that inherit from KeyValueStorage
by changing the table name and the name of the first field in the table.
For example, one can create a module that manages a user preference
table that inherits from KeyValueStorage with only 4 lines of code.

from wsgian.kvs import KeyValueStorage
class Prefs(KeyValueStorage):
    table_name = 'prefs'
    field_name = 'user_id'

"""

from datetime import datetime
import json

from . import mysql_api

__all__ = ["KeyValueStorage"]


class KeyValueStorage:
    table_name = "param"
    field_name = "name"

    @classmethod
    def create_statement(cls):
        sql = f"""
            CREATE TABLE IF NOT EXISTS `{cls.table_name}` (
                `{cls.field_name}`       varchar(40) not null,
                `key`        varchar(40) not null,
                `value`      text,
                `ind`        smallint unsigned not null default 0,
            primary key (`{cls.field_name}`, `key`)
            )
            """
        return (cls.table_name, sql)

    def __init__(self, name):
        self.name = name

    def get(self, key, default=None):
        sql = f"""
            select
                `value`
            from
                `{self.table_name}`
            where
                `{self.field_name}` = %(name)s
                and `key` = %(key)s
            """
        result = mysql_api.select_one(sql, {"name": self.name, "key": key})
        if not result and default is None:
            raise KeyError((self.name, key))
        elif not result:
            return default
        else:
            return json.loads(result["value"])

    def set(self, key, value, ind=0):
        value = json.dumps(value, default=dthandler)
        sql = f"""
            insert into
                `{self.table_name}`
            set
                `{self.field_name}`  = %(name)s,
                `key`   = %(key)s,
                `value` = %(value)s,
                `ind`   = %(ind)s
            on duplicate key update
                `value` = %(value)s,
                `ind`   = %(ind)s
            """
        mysql_api.execute(
            sql, {"name": self.name, "key": key, "value": value, "ind": ind}
        )

    def delete(self, key):
        sql = f"""
            delete
            from
                `{self.table_name}`
            where
                `{self.field_name}` = %(name)s
                and `key` = %(key)s
            """
        mysql_api.execute(sql, {"name": self.name, "key": key})

    def pop(self, key):
        try:
            value = self.get(key)
            self.delete(key)
        except KeyError:
            value = None
        return value

    def dict(self):
        sql = (
            f"select * from {self.table_name} where `{self.field_name}` = %(name)s"
            " order by `ind`"
        )
        result = mysql_api.select(sql, {"name": self.name})
        return {rec["key"]: json.loads(rec["value"]) for rec in result}

    def fix(self):
        """Fix record that are not in json format"""
        sql = f"""\
            select
                *
            from
                {self.table_name}
            where
            `{self.field_name}` = %(name)s"
            order by
                `ind`
        """
        result = mysql_api.select(sql, {"name": self.name})
        for rec in result:
            try:
                value = json.loads(rec["value"])
            except:
                self.set(rec["key"], rec["value"])

    def list(self):
        sql = (
            f"select * from {self.table_name} where `{self.field_name}` = %(name)s"
            " order by `ind`"
        )
        result = mysql_api.select(sql, {"name": self.name})
        for rec in result:
            rec["value"] = json.loads(rec["value"])
        return result

    def save(self, args, set_ind=False):
        for k in args:
            if set_ind and isinstance(k, int):
                self.set(k, args[k], k)
            else:
                self.set(k, args[k])

    def __contains__(self, item):
        return item in self.dict()

    def __iter__(self):
        return iter(self.dict())


def dthandler(obj):
    if hasattr(obj, "isoformat"):
        return obj.isoformat()
    else:
        raise TypeError(
            "Object of type %s with value of %s is not JSON serializable"
            % (type(obj), repr(obj))
        )


def test():
    p = KeyValueStorage(name="topolino")
    p.set("pippo", "cane", 1)
    p.set("pluto", "cane", 2)
    print(p.get("pippo"))
    print(p.get("pluto"))
    try:
        print(p.get("paperino"))
    except KeyError as err:
        print("KeyError", err)

    p.save({"zio paperone": "papero", "nonna papera": "papera"})

    print(p.list())
    print(p.dict())


def test_json_param():
    import pprint

    j = KeyValueStorage(name="gian")
    j.set("dizionario", {"pippo": [1, 2, 3], "pluto": "ciao"})
    j.set("lista", [1, 2, 3, 4])
    j.set("numero", 1)
    j.set("stringa", "Hello")
    j.set("decimale", 1.2)
    j.set("data", datetime.now())

    pprint.pprint(j.list())

    d = j.dict()

    d["dizionario_vuoto"] = {}

    j.save(d)

    pprint.pprint(j.dict())

    print("--------------------")
    print("test pop")
    print("--------------------")
    j.set("messaggio", "ciao mondo")
    print("messaggio:", j.pop("messaggio"))
    print("messaggio:", j.get("messaggio", "Poppato"))
    print("--------------------")


if __name__ == "__main__":
    test()
    test_json_param()
