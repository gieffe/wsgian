"""
This module remains for retro-compatibility. (see kvs.py)
The current version is in the “kvs” module and the “Param” class has
been renamed to the more relevant “KeyValueStorage”.
Methods have been generalized and values are recorded to DB always
in json format.
"""

from . import kvs

__all__ = ["Param", "JsonParam"]


class Param(kvs.KeyValueStorage):
    table_name = "param"
    field_name = "name"


class JsonParam(kvs.KeyValueStorage):
    table_name = "param"
    field_name = "name"


def test_json_param():
    import pprint
    from datetime import datetime

    j = JsonParam(name="gian")
    j.set("dizionario", {"pippo": [1, 2, 3], "pluto": "ciao"})
    j.set("lista", [1, 2, 3, 4])
    j.set("numero", 1)
    j.set("stringa", "Hello")
    j.set("decimale", 1.2)
    j.set("data", datetime.now())

    pprint.pprint(j.list())

    d = j.dict()

    d["dizionario_vuoto"] = {}

    j.save(d)

    pprint.pprint(j.dict())

    print("--------------------")
    print("test pop")
    print("--------------------")
    j.set("messaggio", "ciao mondo")
    print("messaggio:", j.pop("messaggio"))
    print("messaggio:", j.get("messaggio", "Poppato"))
    print("--------------------")


if __name__ == "__main__":
    test_json_param()
