from . import utils

__all_rest__ = [
    "input",
]


class Error(Exception):
    """Base class for other exceptions"""

    pass


def input(pard, args):
    args.setdefault(
        "field", True
    )  # mettere false se stai ridisegnando il campo via ajax
    args.setdefault("id", "")
    args.setdefault("name", "")
    args.setdefault("label", "")
    args.setdefault("label_class", "has-text-centered-touch has-text-weight-normal")
    args.setdefault("placeholder", "")
    args.setdefault("value", "")
    args.setdefault("control_class", "")
    args.setdefault("input_class", "")  # is-danger
    args.setdefault("help_message", "")
    args.setdefault("help_class", "")  # is-success
    args.setdefault("icon_left", "")  # fa-user
    args.setdefault("icon_right", "")  # fa-check
    args.setdefault("disabled", "")  # disabled

    if args["label"] is None:
        pass
    elif not args["label"]:
        args["label"] = args["name"].replace("_", " ").title()
    if args["icon_left"]:
        args["control_class"] += " has-icons-left"
    if args["icon_right"]:
        args["control_class"] += " has-icons-right"

    h = []
    if args["field"]:
        if args["id"]:
            h.append('<div id="%(id)s" class="field">' % args)
        else:
            h.append('<div class="field">' % args)
    if args["label"]:
        h.append('<label class="label %(label_class)s">%(label)s</label>' % args)
    h.append('<div class="control %(control_class)s">' % args)
    h.append(
        '<input class="input %(input_class)s" name="%(name)s" type="text"'
        ' placeholder="%(placeholder)s" value="%(value)s" %(disabled)s>' % args
    )
    if args["icon_left"]:
        h.append(
            '<span class="icon is-small is-left"><i class="fas'
            ' %(icon_left)s"></i></span>' % args
        )
    if args["icon_right"]:
        h.append(
            '<span class="icon is-small is-right"><i class="fas'
            ' %(icon_right)s"></i></span>' % args
        )
    h.append("</div>")
    if args["help_message"]:
        h.append('<p class="help %(help_class)s">%(help_message)s</p>' % args)
    if args["field"]:
        h.append("</div>")

    return "\n".join(h)


def select(pard, args):
    args.setdefault(
        "field", True
    )  # mettere false se stai ridisegnando il campo via ajax
    args.setdefault("id", "")
    args.setdefault("name", "")
    args.setdefault("label", "")
    args.setdefault("options", [])
    args.setdefault("selected", "")
    args.setdefault("control_class", "")
    args.setdefault("select_class", "")  # is-primary
    args.setdefault("help_message", "")
    args.setdefault("help_class", "")  # is-success

    h = []
    if args["field"]:
        if args["id"]:
            h.append('<div id="%(id)s" class="field">' % args)
        else:
            h.append('<div class="field">' % args)

    if args["label"]:
        h.append('<label class="label">%(label)s</label>' % args)

    h.append('<div class="control %(control_class)s">' % args)

    h.append('<div class="select %(select_class)s">' % args)
    h.append('<select name="%(name)s">' % args)
    for opt in args["options"]:
        if opt[0] == args["selected"]:
            h.append('<option value="%s" selected>%s</option>' % opt)
        else:
            h.append('<option value="%s">%s</option>' % opt)
    h.append("</select>")
    h.append("</div>")  # select

    h.append("</div>")  # control

    if args["help_message"]:
        h.append('<p class="help %(help_class)s">%(help_message)s</p>' % args)
    if args["field"]:
        h.append("</div>")  # field

    return "\n".join(h)


def textarea(pard, args):
    args.setdefault(
        "field", True
    )  # mettere false se stai ridisegnando il campo via ajax
    args.setdefault("id", "")
    args.setdefault("name", "")
    args.setdefault("label", "")
    args.setdefault("placeholder", "")
    args.setdefault("value", "")
    args.setdefault("control_class", "")
    args.setdefault("textarea_class", "")
    args.setdefault("help_message", "")
    args.setdefault("help_class", "")  # is-success
    args.setdefault("rows", "")  # 10
    args.setdefault("is-horizontal", False)

    if args["label"] is None:
        pass
    elif not args["label"]:
        args["label"] = args["name"].replace("_", " ").title()

    if args["rows"]:
        args["rows"] = 'rows="%s"' % args["rows"]

    if args["is-horizontal"]:
        h = []

        # Field
        if args["field"]:
            if args["id"]:
                h.append('<div id="%(id)s" class="field is-horizontal">' % args)
            else:
                h.append('<div class="field is-horizontal">' % args)

        # Label
        if args["label"]:
            h.append(
                """
                <div class="field-label is-normal">
                  <label class="label">%(label)s</label>
                </div>
            """
                % args
            )

        # Field Body
        h.append(
            """
            <div class="field-body">
              <div class="field">
                <div class="control %(control_class)s">  
        """
            % args
        )

        # Textarea
        h.append(
            '<textarea class="textarea %(textarea_class)s" %(rows)s name="%(name)s"'
            ' placeholder="%(placeholder)s">%(value)s</textarea>' % args
        )

        # Close Control
        h.append("</div>")

        # Help Message
        if args["help_message"]:
            h.append('<p class="help %(help_class)s">%(help_message)s</p>' % args)

        # Close Field  and Field Body
        h.append("</div>")
        h.append("</div>")

        # Close Horizontal Field
        if args["field"]:
            h.append("</div>")
    else:
        h = []
        if args["field"]:
            if args["id"]:
                h.append('<div id="%(id)s" class="field">' % args)
            else:
                h.append('<div class="field">' % args)
        if args["label"]:
            h.append('<label class="label">%(label)s</label>' % args)
        h.append('<div class="control %(control_class)s">' % args)
        h.append(
            '<textarea class="textarea %(textarea_class)s" %(rows)s name="%(name)s"'
            ' placeholder="%(placeholder)s">%(value)s</textarea>' % args
        )
        h.append("</div>")
        if args["help_message"]:
            h.append('<p class="help %(help_class)s">%(help_message)s</p>' % args)
        if args["field"]:
            h.append("</div>")

    return "\n".join(h)


def notification(pard, args):
    args.setdefault("class", "")  # is-danger, is-info
    args.setdefault("is-light", "is-light")
    args.setdefault("messaggi", [])
    h = []
    h.append('<div class="notification %(class)s %(is-light)s">' % args)
    h.append('<button class="delete"></button>')
    h.append("<br>".join(args["messaggi"]))
    h.append("</div>")
    return "\n".join(h)


def notification_js(pard):
    h = """
        <script>
        document.addEventListener('DOMContentLoaded', () => {
          (document.querySelectorAll('.notification .delete') || []).forEach(($delete) => {
            var $notification = $delete.parentNode;

            $delete.addEventListener('click', () => {
              $notification.parentNode.removeChild($notification);
            });
          });
        });
        </script>
    """
    return h


def img(pard, args):
    args.setdefault("img_class", "is-128x128")
    args.setdefault("img_src", "https://bulma.io/images/placeholders/128x128.png")
    h = []
    h.append('<figure class="image %(img_class)s">' % args)
    h.append('  <img src="%(img_src)s">' % args)
    h.append("</figure>" % args)
    return "\n".join(h)


def card(pard, args):
    args.setdefault("header", "")
    args.setdefault("content", "")
    args.setdefault("footer", [])
    args.setdefault("header_icon", {})

    h = []
    h.append('<div class="card">')
    h.append('<header class="card-header">')
    h.append('<p class="card-header-title">%(header)s</p>' % args)
    if args["header_icon"]:
        h.append(
            """
			<a href="%(href)s" class="card-header-icon" aria-label="%(aria_label)s">
			  <span class="icon">
				<i class="fas %(icon)s" aria-hidden="true"></i>
			  </span>
			</a>
			"""
            % args["header_icon"]
        )
    h.append("</header>")
    h.append('<div class="card-content">')
    h.append(args["content"])
    h.append("</div>")  # card-content
    h.append('<footer class="card-footer">')
    for item in args["footer"]:
        h.append('<a href="%(href)s" class="card-footer-item">%(content)s</a>' % item)
    h.append("</footer>")
    h.append("</div>")  # card
    return "\n".join(h)


def level(pard, args):
    args.setdefault("left", [])
    args.setdefault("right", [])
    h = []
    h.append('<nav class="level">')
    h.append('<div class="level-left">')
    for item in args["left"]:
        h.append('<div class="level-item">')
        h.append(item)
        h.append("</div>")
    h.append("</div>")
    h.append('<div class="level-right">')
    for item in args["right"]:
        h.append('<div class="level-item">')
        h.append(item)
        h.append("</div>")
    h.append("</div>")
    h.append("</nav>")
    return "\n".join(h)


def panel(pard, args):
    args.setdefault("heading", "")
    args.setdefault("tabs", "")
    args.setdefault("panel_blocks", [])
    h = []
    h.append('<nav class="panel">')
    if args["heading"]:
        h.append('<p class="panel-heading">%(heading)s</p>' % args)
    if args["tabs"]:
        h.append("<p>%(tabs)s</p>" % args)
    if args["panel_blocks"]:
        for blk in args["panel_blocks"]:
            h.append(blk)
    h.append("</nav>")

    return "\n".join(h)


def panel_block(pard, args):
    args.setdefault("is-active", "")  # is-active
    args.setdefault("icon", "")  # fas fa-qrcode
    args.setdefault("content", "")
    args.setdefault("href", "#")
    args.setdefault("data", {})
    args["_data"] = _dict_to_data(args["data"])
    h = []
    h.append('<a class="panel-block %(is-active)s" href="%(href)s" %(_data)s>' % args)
    if args["icon"]:
        h.append(
            '<span class="panel-icon"><i class="%(icon)s"'
            ' aria-hidden="true"></i></span>' % args
        )
    h.append(args["content"])
    h.append("</a>")
    return "\n".join(h)


def _dict_to_data(args):
    h = []
    _args = utils.cgi_escape(args)
    for k, v in _args.items():
        h.append(f'data-{k}="{v}"')
    return " ".join(h)


#
# 	h = '''
# <nav class="panel">
#   <p class="panel-heading">
#     Repositories
#   </p>
#   <div class="panel-block">
#     <p class="control has-icons-left">
#       <input class="input" type="text" placeholder="Search">
#       <span class="icon is-left">
#         <i class="fas fa-search" aria-hidden="true"></i>
#       </span>
#     </p>
#   </div>
#   <p class="panel-tabs">
#     <a class="is-active">All</a>
#     <a>Public</a>
#     <a>Private</a>
#     <a>Sources</a>
#     <a>Forks</a>
#   </p>
#   <a class="panel-block is-active">
#     <span class="panel-icon">
#       <i class="fas fa-book" aria-hidden="true"></i>
#     </span>
#     bulma
#   </a>
#   <a class="panel-block">
#     <span class="panel-icon">
#       <i class="fas fa-book" aria-hidden="true"></i>
#     </span>
#     marksheet
#   </a>
#   <a class="panel-block">
#     <span class="panel-icon">
#       <i class="fas fa-book" aria-hidden="true"></i>
#     </span>
#     minireset.css
#   </a>
#   <a class="panel-block">
#     <span class="panel-icon">
#       <i class="fas fa-book" aria-hidden="true"></i>
#     </span>
#     jgthms.github.io
#   </a>
#   <a class="panel-block">
#     <span class="panel-icon">
#       <i class="fas fa-code-branch" aria-hidden="true"></i>
#     </span>
#     daniellowtw/infboard
#   </a>
#   <a class="panel-block">
#     <span class="panel-icon">
#       <i class="fas fa-code-branch" aria-hidden="true"></i>
#     </span>
#     mojs
#   </a>
#   <label class="panel-block">
#     <input type="checkbox">
#     remember me
#   </label>
#   <div class="panel-block">
#     <button class="button is-link is-outlined is-fullwidth">
#       Reset all filters
#     </button>
#   </div>
# </nav>
# 	'''
