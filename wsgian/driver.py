import logging
import string
import time
import traceback
from importlib import reload

from . import utils
from . import auth
from . import sqlite

try:
    from . import mysql
    from . import mysql_api
except ImportError:
    pass


try:
    unicode
except NameError:
    unicode = str


class BadRequest(Exception):
    """when a request paramter value is invalid."""


def check_parameters(pard):
    para = (
        "sid",
        "tmp_sid",
        "module",
        "program",
        "action",
        "sid_user",
        "cookie_sid_user",
    )
    try:
        ts = str.maketrans("_.-", "xxx")
    except AttributeError:
        ts = string.maketrans("_.-", "xxx")
    tu = dict(list(zip(list(map(ord, "_.-")), "xxx")))
    for key in para:
        if key in pard and pard[key]:
            if isinstance(pard[key], str):
                x = pard[key].translate(ts)
            elif isinstance(pard[key], unicode):
                x = pard[key].translate(tu)
            else:
                logging.error((key, pard[key]))
                raise BadRequest()
            if not x.isalnum():
                logging.error((key, pard[key]))
                raise BadRequest()


def driver(pard):
    start = time.time()
    result = {
        "html": "",
        "header": [("Content-type", "text/html; charset=utf-8")],
        "set_cookie": {},  # Dictionary
        "delete_cookie": [],  # List
    }
    try:
        check_parameters(pard)
        # pard = config.config(pard)

        if "db.sqlite" in pard and pard["db.sqlite"]:
            pard["sqlite.conn"] = sqlite.DB(**pard["db.sqlite"])

        if "db.mysql" in pard and pard["db.mysql"]:
            pard["mysql.conn"] = mysql.Connection(**pard["db.mysql"])

        ## get pard['AUTH_MENU'] and pard['USER_DICT']
        # pard = auth.load_user_data(pard)

        pard.setdefault("sid_user", "")
        if not pard["sid_user"] and "cookie_sid_user" in pard:
            pard["sid_user"] = pard["cookie_sid_user"]

        pard["user_data"] = {}

        if pard["sid_user"] and "AUTH_MODULE" in pard and pard["AUTH_MODULE"]:
            pard["user_data"] = __import__(
                pard["AUTH_MODULE"], fromlist=["pippo"]
            ).__dict__["get_user"](pard, pard["sid_user"])

        if pard["login"] and not pard["user_data"]:
            pard["module"] = pard.get("LOGIN_MODULE", "login")
            pard["action"] = "login"
            pard["continue"] = pard["HTTP_URL"]
        elif (
            pard["login"]
            and pard["user_data"]
            and pard["user_data"]["status"] == "temporary"
            and pard.get("module", "") != pard.get("LOGIN_MODULE", "login")
        ):
            pard["module"] = pard.get("LOGIN_MODULE", "login")
            pard["action"] = "confirm_account"
            pard["continue"] = pard["HTTP_URL"]
        elif not auth.orized(pard):
            pard["module"] = pard.get("LOGIN_MODULE", "login")
            pard["action"] = "not_authorized"

        if pard.get("DEBUG"):
            reload_developing_modules(pard)

        pard = __import__(pard["module"], fromlist=["pippo"]).__dict__["main"](pard)

        ## ToDo controllo del result proveniente dall'applicazione
        if "html" in pard:
            result["html"] = pard["html"]  # deve essere una stringa o unicode
        if "status" in pard:
            result["status"] = pard["status"]  # deve essere un http status code
        if "header" in pard:
            result["header"] = pard["header"]  # deve essere una lista di tuple
        if "redirect" in pard:
            result["redirect"] = pard["redirect"]  # Stringa
        if "serve_static" in pard:
            result["serve_static"] = pard["serve_static"]  # file_path
        if "set_cookie" in pard:
            result["set_cookie"] = pard["set_cookie"]  # Dizionario dei cookie
        if "set_cookie_expires_days" in pard:
            # Scadenza del cookie [numero o None]
            result["set_cookie_expires_days"] = pard["set_cookie_expires_days"]
        if "delete_cookie" in pard:
            result["delete_cookie"] = pard["delete_cookie"]  # lista di k

        stop = time.time()
        pard["exec_time"] = repr(stop - start)[0:5]
        auth.log_hit(pard)
        return result

    except BadRequest:
        result["status"] = 400
        result["header"] = [("Content-type", "text/html; charset=utf-8")]
        result["html"] = auth.build_bad_request(pard)
        return result

    except Exception as e:
        stop = time.time()
        logging.error(traceback.format_exc())
        pard["exec_time"] = repr(stop - start)[0:5]
        application_error = False
        try:
            auth.log_error(pard)
            pard.setdefault("APP_ERROR_LIST", ())
            if type(e) in pard["APP_ERROR_LIST"]:
                application_error = True
                error_message = f"{type(e).__name__}: {str(e)}"
        except:
            pass
        result["status"] = 500
        result["header"] = [("Content-type", "text/html; charset=utf-8")]
        if application_error:
            result["html"] = auth.build_application_error(pard, error_message)
        else:
            result["html"] = auth.build_internal_server_error(pard)
        return result
    finally:
        try:
            if "ATEXIT" in pard:
                pard["ATEXIT"](pard)
        except:
            logging.error("Error executing ATEXIT procedure")
            logging.error(traceback.format_exc())
        try:
            if "mysql.conn" in pard:
                mysql_api.close_connections()
                del pard["mysql.conn"]
            if "sqlite.conn" in pard:
                pard["sqlite.conn"].close()
            del pard
        except NameError:
            pass


def reload_developing_modules(pard):
    if "BASE_DIR" in pard:
        fn = pard["BASE_DIR"] + "/developing_modules"
    else:
        fn = utils.sib_path(__name__, "developing_modules")
    try:
        buf = open(fn).read()
    except OSError:
        return []
    modules_list = buf.split()
    for item in modules_list:
        cmd = "import {};reload({})".format(item, item)
        exec(cmd)
    return modules_list
