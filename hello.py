import wsgian


def main(pard):
    pard.setdefault("who", "wsgian")
    return {"html": "Hello %(who)s!" % pard}


urls = [
    {"pattern": "/{who:\w+}", "module": "hello"},
    {"pattern": "/", "module": "hello"},
]

config = {
    "with_static": False,
    "DEBUG": True,
    "LOGLEVEL": "debug",
    "cookie_secret": "substitutewithyourcookiesecret",
}

app = wsgian.App(urls, config)

if __name__ == "__main__":
    wsgian.quickstart(app)
