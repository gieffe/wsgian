"""
Account Interface Module

ToDo:
    - change_email
"""

import functools
from .. import validate
from ..errors import ErrorList, LoginError
from .. import utils
from .. import mysql

__author__ = "gmessori@gmail.com (Gianfranco Messori)"
__all__ = []


# ------------------------------------------------------------------------- #
# Utility
# ------------------------------------------------------------------------- #
def _data_dict_check(function):
    @functools.wraps(function)
    def wrapper(pard, data={}):
        if not isinstance(data, dict):
            return {
                "errors": ["data must be dictionary, %s found" % type(data).__name__]
            }
        else:
            return function(pard, data)

    return wrapper


# ------------------------------------------------------------------------- #

project_tables = {
    "account": """
        create table account (
            userid     varchar(32)   NOT NULL default '',
            fullname   varchar(255)  NOT NULL default '',
            email      varchar(80)   NOT NULL default '',
            password   varchar(50)   NOT NULL default '',
            status     varchar(20)   NOT NULL default '',
            token      varchar(32)   NOT NULL default '',
            secret_key varchar(50)   NOT NULL default '',
            new_email  varchar(80)   NOT NULL default '',
            provider   varchar(50)   NOT NULL default '',
            upd_ts     timestamp NOT NULL default '' default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
            PRIMARY KEY  (userid),
            UNIQUE KEY email (email)
            )
        """,
    "account_msg": """
        create table account_msg (
            msgid    int(15)     unsigned NOT NULL auto_increment,
            userid   varchar(32) NOT NULL default '',
            msg      text        NOT NULL,
            type     varchar(20) NOT NULL default '',
            status   varchar(20) NOT NULL default 'unread',
            upd_ts     timestamp NOT NULL default '' default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
            PRIMARY KEY (msgid),
            KEY userid (userid)
        )
        """,
}
# ------------------------------------------------------------------------- #


def check(pard, data):
    data.setdefault("fullname", "")
    data.setdefault("email", "")
    data.setdefault("password", "")
    data.setdefault("status", "temporary")
    data.setdefault("provider", "")
    errors = []
    if data["provider"] == "google":
        if "userid" in data and data["userid"]:
            pass
        else:
            errors.append("Missing `userid` for google account")
        data["password"] = utils.get_token() + "-google"
    else:
        try:
            data["password"] = validate.hash(data["password"], "password")
        except Exception as err:
            errors.append(str(err))
    try:
        data["email"] = validate.email(data["email"])
    except Exception as err:
        errors.append(str(err))
    if data["status"] not in ("temporary", "active", "disabled"):
        errors.append("Incorrect Account status %s" % data["status"])

    if errors:
        raise ErrorList(errors)

    if not errors:
        if get_by_email(pard, data["email"]):
            raise ErrorList("Address `%s` already in use" % data["email"])

    return data


@_data_dict_check
def create(pard, data):
    """Create new account and send email confirmation

    Args:
        data['email']
        data['password']
        data['fullname'] optional
        data['status'] optional default 'temporary'
        data['provider'] optional

    Returns:
        {'userid': ..., 'errors': []}

    """
    resd = {"userid": "", "errors": []}

    try:
        data = check(pard, data)
    except ErrorList as err:
        resd["errors"] = err.value
        return resd

    if data["provider"] != "google":
        data["userid"] = resd["userid"] = utils.get_token()
    else:
        resd["userid"] = data["userid"]

    sql = """
        INSERT INTO account
        (userid, fullname, email, password, status, provider)
        VALUES
        (%(userid)s, %(fullname)s,
         %(email)s, %(password)s, %(status)s, %(provider)s)
        """
    pard["mysql.conn"].execute(sql, data)

    return resd


def _delete(pard, userid):
    sql = "delete from account where userid = %(userid)s"
    result = pard["mysql.conn"].execute(sql, {"userid": userid})
    return result


def get(pard, userid):
    sql = "select * from account where userid = %(userid)s"
    result = pard["mysql.conn"].get(sql, {"userid": userid})
    return result


def get_user(pard, userid):
    sql = """
        select userid, fullname, email, status, token,
               secret_key, new_email, provider
        from   account where userid = %(userid)s
        and    status in ('temporary', 'active')
        """
    result = pard["mysql.conn"].get(sql, {"userid": userid})
    return result


def get_by_email(pard, email):
    sql = "select * from account where email = %(email)s"
    result = pard["mysql.conn"].get(sql, {"email": email})
    return result


def get_by_token(pard, token):
    if not token:
        token = "###"
    sql = "select * from account where token = %(token)s"
    result = pard["mysql.conn"].get(sql, {"token": token})
    return result


@_data_dict_check
def check_login(pard, data):
    """Validate login request

    Args:
        data['email']
        data['password']

    Returns:
        {'act_data': {}, 'errors': []}
    """
    resd = {"act_data": {}, "errors": []}
    data.setdefault("email", "")
    data.setdefault("password", "")
    try:
        try:
            data["password"] = validate.hash(data["password"], "password")
        except Exception:
            raise LoginError()
        try:
            data["email"] = validate.email(data["email"])
        except Exception:
            raise LoginError()
        sql = """
            select * from account
            where password = %(password)s
            and   email = %(email)s
            and   provider = ''
            """
        result = pard["mysql.conn"].get(sql, data)
        if not result:
            raise LoginError()
        elif result["status"] == "disabled":
            raise LoginError()
    except LoginError as err:
        resd["errors"] = [err.value]  # , data]
    else:
        resd["act_data"] = result

    return resd


@_data_dict_check
def get_list(pard, data=None):
    """Get Accounts list

    Args:
        data['limit'] optional default 20
        data['offset'] optional default 0

    Returns:
        [list of account data dictionary]
    """
    if not data:
        data = {}
    data.setdefault("limit", 20)
    data.setdefault("offset", 0)
    data["limit"] = validate.to_int(data["limit"], "limit")
    data["offset"] = validate.to_int(data["offset"], "offset")
    sql = """
        select *
        from   account
        order by email
        limit %(offset)s, %(limit)s
        """
    result = pard["mysql.conn"].query(sql, data)
    return result


def user_list(pard, token):
    """Search user account that match token in email or fullname

    Args:
        token

    Returns:
        [{'userid': '...', 'fullname', '...', 'email': '...'}]

    """
    _token = pard["mysql.conn"].escape_string(token)
    sql = """
        select userid, fullname, email
        from   account
        where  fullname like '%{}%'
        or     email like '%{}%'
        """.format(
        _token,
        _token,
    )
    result = pard["mysql.conn"].query(sql)
    return result


def new_token(pard, userid):
    token = utils.get_token()
    sql = "update account set token = '{}' where userid = '{}'".format(token, userid)
    pard["mysql.conn"].execute(sql)
    return token


def reset_token(pard, token):
    if not token:
        token = "###"
    sql = "update account set token = '' where token = %(token)s"
    pard["mysql.conn"].execute(sql, {"token": token})
    return "Done"


@_data_dict_check
def confirm_account(pard, data):
    """Confirm account by token

    Input:
        data['token']
        data['action'] ['confirm', 'reject']

    Output:
        {'errors': []}

    """
    data.setdefault("token", "###")
    data.setdefault("action", "confirm")
    resd = {"errors": []}
    if data["token"] == "":
        data["token"] = "###"
    act = get_by_token(pard, data["token"])

    if not act:
        resd["errors"] = ["Expired token."]
    elif data["action"] == "confirm":
        sql = """
            update account set
                status = 'active',
                token= ''
            where userid = %(userid)s
            """
        pard["mysql.conn"].execute(sql, act)
        add_user_msg(
            pard,
            {
                "userid": act["userid"],
                "msg": "Your accaunt has been confirmed. Thanks!",
            },
        )
        resd["msg"] = ["Your accaunt has been confirmed. Thanks!"]
    elif data["action"] == "reject":
        sql = """
            update account set
                status = 'disabled',
                token= ''
            where userid = %(userid)s
            """
        pard["mysql.conn"].execute(sql, act)
        resd["errors"] = ["The Account has been successfully deleted"]
    else:
        resd["errors"] = ["Invalid action"]

    return resd


@_data_dict_check
def change_fullname(pard, data):
    """Change fullname

    Args:
        data['userid'],
        data['fullname']

    Returns:
        {'errors': []}
    """
    resd = {"errors": []}
    data.setdefault("userid", "")
    data.setdefault("fullname", "")
    act = get(pard, data["userid"])
    if not act:
        resd["errors"] = ["Account not found"]
        return resd
    sql = "update account set fullname = %(fullname)s where userid = %(userid)s"
    pard["mysql.conn"].execute(sql, data)
    return resd


def disable_account(pard, userid, undo=False):
    """Disable account (with undo option to re-activate)

    Args:
        userid,
        undo default False

    Returns:
        String message
    """
    data = {"userid": userid}
    if undo:
        data["status"] = "active"
    else:
        data["status"] = "disabled"
    pard["mysql.conn"].execute(
        """
        update account set
            status = %(status)s
        where userid = %(userid)s
        """,
        data,
    )
    return "Account %(userid)s %(status)s" % data


@_data_dict_check
def change_password(pard, data):
    """Change Password

    Args:
        data['userid']
        data['old_password']
        data['new_password'],
        data['new_password_again']

    Returns:
        {'errors': []}

    """
    resd = {"errors": []}
    data.setdefault("userid", "")
    data.setdefault("old_password", "")
    data.setdefault("new_password", "")
    data.setdefault("new_password_again", "")

    act = get(pard, data["userid"])
    if not act:
        resd["errors"] = ["Account not found"]
        return resd

    result = check_login(
        pard, {"username": act["username"], "password": data["old_password"]}
    )
    if result["errors"]:
        resd["errors"] = ["The old password was incorrect"]
        return resd

    try:
        data["new_password"] = validate.hash(data["new_password"], "new_password")
        data["new_password_again"] = validate.hash(
            data["new_password_again"], "new_password_again"
        )
    except Exception as err:
        resd["errors"] = [str(err)]
        return resd

    if data["new_password"] != data["new_password_again"]:
        resd["errors"] = ["Passwords must match."]
        return resd

    sql = "update account set password = %(new_password)s where userid = %(userid)s"
    pard["mysql.conn"].execute(sql, data)

    return resd


@_data_dict_check
def change_password_with_token(pard, data):
    """Change Password

    Args:
        data['token']
        data['new_password'],
        data['new_password_again']
        data['fullname'] optional

    Returns:
        {'errors': []}
    """
    resd = {"errors": []}
    data.setdefault("token", "###")
    data.setdefault("new_password", "")
    data.setdefault("new_password_again", "")

    act = get_by_token(pard, data["token"])
    if not act:
        resd["errors"] = ["Expired token."]
        return resd
    data["userid"] = act["userid"]
    if "fullname" in data and data["fullname"]:
        pass
    else:
        data["fullname"] = act["fullname"]

    try:
        data["new_password"] = validate.hash(data["new_password"], "new_password")
        data["new_password_again"] = validate.hash(
            data["new_password_again"], "new_password_again"
        )
    except Exception as err:
        resd["errors"] = [str(err)]
        return resd

    if data["new_password"] != data["new_password_again"]:
        resd["errors"] = ["Passwords must match."]
        return resd

    sql = """
        update account set
            password = %(new_password)s,
            token = '',
            status = 'active',
            fullname = %(fullname)s
        where userid = %(userid)s
        """
    pard["mysql.conn"].execute(sql, data)

    return resd


# @_data_dict_check
# def reset_password(pard, data):
#     """Send email with a link to reset password
#
#     Args:
#         data['email'],
#
#     Returns:
#         {'errors': [], 'msg': ''}
#
#     """
#     resd = {'errors': []}
#     data.setdefault('email', '')
#     try:
#         data['email'] = wsgian.validate.email(data['email'])
#     except Exception:
#         resd['errors'] = ['invalid email address']
#         return resd
#     act = get_by_email(pard, data['email'])
#     if not act:
#         resd['errors'] = ['There\'s no account asocieted with that email address.']
#         return resd
#     pard['token'] = new_token(pard, act['userid'])
#     pard['username'] = act['username']
#     message_body = """
# Hello %(username)s,
#
# We heard you need a password reset for %(APPLICATION_NAME)s. Click the link below
# and you'll be redirected to a secure site from which you can set a
# new password.
#
# Reset Password: %(APPSERVER)s%(ACT_RESET_PASSWORD)s%(token)s
#
#
# If you didn't try to reset your password,
# click here: %(APPSERVER)s%(ACT_RESET_PASSWORD)s%(token)s?reset=false
# and we'll forget this ever happened.
#
# """
#     try:
#         message_body = message_body % pard
#         sender = pard['DEFAULT_SENDER']
#     except KeyError, err:
#         resd['errors'] = ['Email is not properly configured (missing %s)' % err]
#         return resd
#
#
#     message = mail.EmailMessage(
#         sender = pard['DEFAULT_SENDER'],
#         to = '%s <%s>' % (act['fullname'], act['email']),
#         subject='%s Password Reset' % pard['APPLICATION_NAME'],
#         body=message_body)
#
#     message.send(pard)
#     if message.err:
#         resd['errors'] = ['Error sending email']
#     else:
#         resd['msg'] = 'We have sent you an email with instruction for changing your password.'
#     return resd
#
# @_data_dict_check
# def change_email(pard, data):
#     """Send email confirmation for changing account email address.
#
#     Args:
#         data['userid'],
#         data['new_email'],
#
#     Returns:
#         {'errors': [], 'msg': ''}
#
#     """
#     resd = {'errors': [], 'msg': ''}
#     data.setdefault('userid', '')
#     data.setdefault('new_email', '')
#
#     try:
#         data['new_email'] = wsgian.validate.email(data['new_email'])
#     except Exception:
#         resd['errors'] = ['invalid email address']
#         return resd
#
#     act = get(pard, data['userid'])
#     if not act:
#         resd['errors'] = ['Account not found']
#         return resd
#
#     sql = """
#         update account set
#             new_email = %(new_email)s
#         where
#             userid = %(userid)s
#         """
#     pard['mysql.conn'].execute(
#         sql, {'userid': act['userid'], 'new_email': data['email']})
#
#     pard['token'] = new_token(pard, act['userid'])
#     pard['username'] = act['username']
#     pard['email'] = act['email']
#     pard['new_email'] = data['new_email']
#
#     message_body = """
# %(APPLICATION_NAME)s - Confirm Email Change
#
# You have submitted a request to change your email address from
# %(email)s to %(new_email)s.
#
# Click the link below to verify the change to your email address:
# %(APPSERVER)s%(ACT_CHANGE_EMAIL)s%(token)s
#
#
# To cancel the change request, click here:
# %(APPSERVER)s%(ACT_CHANGE_EMAIL)s%(token)s?change=false
#
# """
#     try:
#         message_body = message_body % pard
#         sender = pard['DEFAULT_SENDER']
#     except KeyError, err:
#         resd['errors'] = ['Email is not properly configured (missing %s)' % err]
#         return resd
#
#     message = mail.EmailMessage(
#         sender = pard['DEFAULT_SENDER'],
#         to = '%s <%s>' % (act['fullname'], act['email']),
#         subject='Email Address Change',
#         body=message_body)
#
#     message.send(pard)
#     if message.err:
#         resd['errors'] = ['Error sending email']
#     else:
#         resd['msg'] = 'This change will take effect after you click on the link in the confirmation email sent to the new email address.'
#     return resd


# ------------------------------------------------------------------------- #
# User Messages
# ------------------------------------------------------------------------- #
@_data_dict_check
def add_user_msg(pard, data):
    """Add message for the user account

    Args:
        data['userid'],
        data['msg'],
        data['type'] default='once' (one shot message)
                     ['once', 'notification', 'email']

    Returns:
        {'errors': [], 'msg': data['msg'], 'msgid': ''}
    """
    data.setdefault("userid", "")
    data.setdefault("msg", "")
    data.setdefault("type", "once")
    resd = {"errors": [], "msg": data["msg"]}
    act = get(pard, data["userid"])
    if not act:
        resd["errors"].append("I can't find this account: %(userid)s" % data)
    elif not data["msg"]:
        resd["errors"].append("What about tell something to %(username)s" % act)
    if resd["errors"]:
        return resd
    sql = """
        insert into account_msg
            (userid, msg, type)
        values
            (%(userid)s, %(msg)s, %(type)s)
        """
    resd["msgid"] = pard["mysql.conn"].execute(sql, data)
    return resd


def get_user_msg(pard, userid):
    sql = """
        select * 
        from   account_msg
        where  userid = %(userid)s
        order by status, msgid
        """
    return pard["mysql.conn"].query(sql, {"userid": userid})


def pop_user_msg(pard, userid):
    """Pop the list of unread messages of type "once" for user data['userid']

    Args:
        userid

    Results:
        {'errors': [], 'msg_list': []}

    """
    resd = {"errors": [], "msg_list": []}
    sql = """
        select msg
        from   account_msg
        where  userid = %(userid)s
        and    type = 'once'
        and    status = 'unread'
        order by msgid
        """
    result = pard["mysql.conn"].query(sql, {"userid": userid})
    if result:
        resd["msg_list"] = [rec["msg"] for rec in result]
        sql = """
            update account_msg
            set    status = 'read'
            where  userid = %(userid)s
            and    type = 'once'
            and    status = 'unread'
            """
        pard["mysql.conn"].execute(sql, {"userid": userid})
    return resd


# -------------------------------------------------------------------------- #
def get_table_list(pard):
    pard["sql"] = "show tables"
    result = pard["mysql.conn"].send(pard["sql"])
    return [item[0] for item in result]


def test_database(pard):
    pard["mysql.conn"] = mysql.Connection(**pard["db.mysql"])
    db_table_list = get_table_list(pard)
    # print project_tables.keys()
    # print db_table_list
    for table in list(project_tables.keys()):
        if table not in db_table_list:
            print("Create Table %s" % table)
            pard["mysql.conn"].execute(project_tables[table])
    desc_account = pard["mysql.conn"].query("show columns from account")
    for rec in desc_account:
        if rec["Field"] == "provider":
            break
    else:
        pard["mysql.conn"].execute(
            "alter table account add column provider varchar(50) not null default ''"
        )
        # print 'add column priveder'
    pard["mysql.conn"].close()


# -------------------------------------------------------------------------- #


# ------------------------------------------------------------------------- #
# Update __all__ to contain all function that doesn't starts with '_'
__all__ = []
for _name, _object in list(globals().items()):
    if not _name.startswith("_") and callable(_object):
        __all__.append(_name)
# ------------------------------------------------------------------------- #


if __name__ == "__main__":
    import app
    from .. import mysql

    pard = app.config
    pard["mysql.conn"] = mysql.Connection(**pard["db.mysql"])
    test_database(pard)
