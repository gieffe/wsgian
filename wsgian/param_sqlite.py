"""
Copy of wsgian.param with sqlite sql syntax
"""

from sqlite3 import IntegrityError

__all__ = ["Param"]


class Param:
    table_name = "param"
    field_name = "name"

    @classmethod
    def create_statement(cls):
        sql = f"""
            CREATE TABLE IF NOT EXISTS `{cls.table_name}` (
                `{cls.field_name}`       varchar(40) not null,
                `key`        varchar(40) not null,
                `value`      text,
                `ind`        smallint unsigned not null default 0,
            primary key (`{cls.field_name}`, `key`)
            )
            """
        return (cls.table_name, sql)

    def __init__(self, sql_conn, name):
        self.sql_conn = sql_conn
        self.name = name

    def create_table(self):
        table, sql = self.create_statement()
        print(f"CREATE TABLE IF NOT EXISTS {table}")
        self.sql_conn.execute(sql)

    def get(self, key, default=None):
        sql = f"""
            select
                `value`
            from
                `{self.table_name}`
            where
                `{self.field_name}` = :name
                and `key` = :key
            """
        result = self.sql_conn.query(sql, {"name": self.name, "key": key})
        if not result and default is None:
            raise KeyError((self.name, key))
        elif not result:
            return default
        else:
            return result[0]["value"]

    def set(self, key, value, ind=0):
        sql = f"""
            insert into
                `{self.table_name}` (
                    `{self.field_name}`,
                    `key`,
                    `value`,
                    `ind`
                )
            values
                (
                    :name,
                    :key,
                    :value,
                    :ind
                )
            """
        try:
            self.sql_conn.execute(
                sql, {"name": self.name, "key": key, "value": value, "ind": ind}
            )
        except IntegrityError:
            sql = f"""
                update
                    `{self.table_name}`
                set
                    `value` = :value,
                    `ind` = :ind
                where
                    `{self.field_name}` = :name
                    and `key` = :key
            """
            self.sql_conn.execute(
                sql, {"name": self.name, "key": key, "value": value, "ind": ind}
            )
        self.sql_conn.commit()

    def delete(self, key):
        sql = f"""
            delete
            from
                `{self.table_name}`
            where
                `{self.field_name}` = :name
                and `key` = :key
            """
        self.sql_conn.execute(sql, {"name": self.name, "key": key})
        self.sql_conn.commit()

    def dict(self):
        sql = (
            f"select * from {self.table_name} where `{self.field_name}` = :name order"
            " by `ind`"
        )
        result = self.sql_conn.query(sql, {"name": self.name})
        return {rec["key"]: rec["value"] for rec in result}

    def list(self):
        sql = (
            f"select * from {self.table_name} where `{self.field_name}` = :name order"
            " by `ind`"
        )
        result = self.sql_conn.query(sql, {"name": self.name})
        return result

    def save(self, args):
        for k in args:
            self.set(k, args[k])


def test():
    from wsgian import sqlite

    sql_conn = sqlite.connect()

    p = Param(sql_conn, "pippo")
    p.create_table()
    p.set("pippo", "numero 1", 1)
    p.set("pluto", "numero 2", 2)
    print(p.get("pippo"))
    print(p.get("pluto"))
    try:
        print(p.get("paperino"))
    except KeyError as err:
        print("KeyError", err)

    p.save({"zio paperone": "$$$", "nonna papera": "numero 0"})

    print(p.list())
    print(p.dict())


def test_subclass():
    import pprint
    from wsgian import sqlite

    sql_conn = sqlite.connect()

    class Prefs(Param):
        table_name = "prefs"
        field_name = "user_id"

    q = Prefs(sql_conn, "pluto")
    q.create_table()

    q.save(Param(sql_conn, "pippo").dict())

    pprint.pprint(q.list())

    sql_conn.close()


if __name__ == "__main__":
    test()
    test_subclass()
