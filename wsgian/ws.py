from . import json
import logging
import pprint
import traceback
from . import utils


try:
    unicode
except NameError:
    unicode = str


def main(pard):
    version = pard.get("ws_version", 1)
    if version in (1, "1"):
        return main_1(pard)
    elif version in (2, "2"):
        return main_2(pard)
    else:
        return main_3(pard)


def main_1(pard):
    pard.setdefault("ws_module", "")
    pard.setdefault("ws_action", "")
    __all_rest__ = __import__(pard["ws_module"], fromlist=["pippo"]).__all_rest__

    if pard["ws_action"] in __all_rest__:
        action = __import__(pard["ws_module"], fromlist=["pippo"]).__dict__[
            pard["ws_action"]
        ]
        paramd = utils.cgi_params(pard, "paramd")
        params = pard.get("params", None)
        if isinstance(params, unicode) or isinstance(params, str):
            params = [params]
        logging.info("### Parametri:\n")
        logging.info("paramd: %s" % pprint.pformat(paramd))
        logging.info("params: %s\n" % params)
        try:
            if paramd:
                result = action(pard, paramd)
            elif params:
                result = action(pard, *params)
            else:
                result = action(pard)
        except Exception as err:
            pard["status"] = 400
            pard["html"] = traceback.format_exc()  # str(err)
        else:
            pard["header"] = [("Content-type", "application/json; charset=utf-8")]
            pard["html"] = json.encode_date_to_iso(result)

    else:
        pard["status"] = 400
        pard["html"] = "Wrong Action"

    return pard


def main_2(pard):
    pard.setdefault("ws_module", "")
    pard.setdefault("ws_action", "")
    __all_rest__ = __import__(pard["ws_module"], fromlist=["pippo"]).__all_rest__

    if pard["ws_action"] in __all_rest__:
        action = __import__(pard["ws_module"], fromlist=["pippo"]).__dict__[
            pard["ws_action"]
        ]
        paramd = pard["request_parmeters"]
        logging.info("paramd: %s" % pprint.pformat(paramd))
        try:
            if paramd:
                result = action(pard, paramd)
            else:
                result = action(pard)
        except __import__(pard["ws_module"], fromlist=["pippo"]).__dict__[
            "Error"
        ] as err:
            result = {
                "status": "Error",
                "error_message": str(err),
                "error_class": err.__class__.__name__,
            }
            pard["header"] = [("Content-type", "application/json; charset=utf-8")]
            pard["html"] = json.encode_date_to_iso(result)
        except ValueError as err:
            result = {
                "status": "Error",
                "error_message": str(err),
                "error_class": "ValueError",
            }
            pard["header"] = [("Content-type", "application/json; charset=utf-8")]
            pard["html"] = json.encode_date_to_iso(result)
        except Exception as err:
            pard["status"] = 400
            err = traceback.format_exc()
            pard["html"] = "<pre>%s</pre>" % err
        else:
            pard["header"] = [("Content-type", "application/json; charset=utf-8")]
            pard["html"] = json.encode_date_to_iso(result)

    else:
        pard["status"] = 400
        pard["html"] = "Wrong Action"

    return pard


def main_3(pard):
    pard.setdefault("ws_module", "")
    pard.setdefault("ws_action", "")
    pard.setdefault("ws_user_key", "user_id")
    __all_rest__ = __import__(pard["ws_module"], fromlist=["pippo"]).__all_rest__

    if pard["ws_action"] in __all_rest__:
        action = __import__(pard["ws_module"], fromlist=["pippo"]).__dict__[
            pard["ws_action"]
        ]
        paramd = pard["request_parmeters"]
        if pard.get("login", True):
            paramd[pard["ws_user_key"]] = pard.get("sid_user", "")
        logging.info("paramd: %s" % pprint.pformat(paramd))
        try:
            if paramd:
                result = action(pard, paramd)
            else:
                result = action(pard)
        except __import__(pard["ws_module"], fromlist=["pippo"]).__dict__[
            "Error"
        ] as err:
            result = {
                "status": "Error",
                "error_message": str(err),
                "error_class": err.__class__.__name__,
            }
            pard["header"] = [("Content-type", "application/json; charset=utf-8")]
            pard["html"] = json.encode_date_to_iso(result)
        except ValueError as err:
            result = {
                "status": "Error",
                "error_message": str(err),
                "error_class": "ValueError",
            }
            pard["header"] = [("Content-type", "application/json; charset=utf-8")]
            pard["html"] = json.encode_date_to_iso(result)
        except Exception as err:
            pard["status"] = 400
            err = traceback.format_exc()
            pard["html"] = "<pre>%s</pre>" % err
        else:
            pard["header"] = [("Content-type", "application/json; charset=utf-8")]
            pard["html"] = json.encode_date_to_iso(
                {"status": "Success", "result": result}
            )

    else:
        pard["status"] = 400
        pard["html"] = "Wrong Action"

    return pard


def test(url, d):
    """
    from wsgian.ws import test

    test('http://localhost:8080/api/entity/methods',
             {'id': '00099900', 'pippo': 'pluto'})
    """
    import requests

    resp = requests.get(url, d)
    try:
        result = resp.json()
    except ValueError:
        result = resp.content
    pprint.pprint(result)
