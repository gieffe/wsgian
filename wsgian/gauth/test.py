import app
import webob
import sys


def create_user(email="pippo@pluto.com", passw="pippopluto"):
    d = {}
    d["email"] = email
    d["password"] = passw
    # d['fullname'] = 'Gianfranco Messori'
    req = webob.Request.blank("/1/acct/create", POST=d)
    res = req.get_response(app.app)
    print_resp(res)


def check_login(email="pippo@pluto.com", passw="pippopluto"):
    d = {}
    d["email"] = email
    d["password"] = passw
    req = webob.Request.blank("/1/acct/check", POST=d)
    res = req.get_response(app.app)
    print_resp(res)


def get_user(userid):
    req = webob.Request.blank("/1/acct/get/%s" % userid)
    res = req.get_response(app.app)
    print_resp(res)


def get_list():
    req = webob.Request.blank("/1/acct/list")
    res = req.get_response(app.app)
    print_resp(res)


def print_resp(res):
    print(res.body)
    print(res.headers)
    print(res.status)


def main():

    if len(sys.argv) == 1:
        print("usage: python %s action [optional argument]" % __file__)
        sys.exit(0)

    action = sys.argv[1]

    if action == "get_user":
        userid = sys.argv[2]
        get_user(userid)

    elif action == "create_user":
        if len(sys.argv) > 2:
            email = sys.argv[2]
            passw = sys.argv[3]
            create_user(email, passw)
        else:
            create_user()

    elif action == "check_login":
        email = sys.argv[2]
        passw = sys.argv[3]
        check_login(email, passw)

    elif action == "get_list":
        get_list()

    else:
        print("wrong action")
        sys.exit(0)


if __name__ == "__main__":
    main()
