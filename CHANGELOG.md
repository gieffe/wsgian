# Changelog

## [Unreleased]

### Added

- Aggiunto il metodo `fix` alla classe `KeyValueStorage` che permette di sistemare dei
  record presenti sul database, registrati dalla versione precedente (`Params`) non
  in formato `json`
- Aggiunto il filtro `dump` al default environment dei template
- Aggiunto `render_template_block` al modulo `template`, utilizzando il package
  `jinja2_fragments` (aggiunto anche nelle dipendendenze).
  Permette di utilizzare come template solo il contenuto di un *blocco* di un template
  più grande. Possono essere anche più blocchi dello stesso template, utile ad esempio,
  quando si usa la libreria `htmx` e si usa la tecnica degli external object swap
  `hx-swap-oob="true"`

```python
from wsgian.template import render_template_block

# render_template_block(nome_template, nome_blocco, **context)
html = render_template_block("editor.html", "last_update", **context)
```

- Aggiunta la possibilità di aggiungere filtri all'environment dei template
  
```python
def uppero(s):
    s.upper()

template.add_filter("uppero", uppero)

```

- Aggiunte alcune variabili globali al templete environment
```python
env.globals.update(
    {
        "APPSERVER": pard["APPSERVER"],
        "TITLE": pard["TITLE"],
        "LOGOUT_URL": pard["LOGOUT_URL"],
        "url_for": url_for,
    }
)
```

- aggiunta la *funzione* `url_for` che fa reverse mapping di *module/action*
  e può (deve) essere utilizzata dai programmi e dai template per ottenere le url
  corrette servite dall'applcazione.

```python
from wsgian import App, url_for

urls = [
    {
        "pattern": r"/app/notes/{note_id:\w+}/save_content",
        "module": "webnotes.editor",
        "action": "save_content",
        "login": True,
    },
]
app = App(urls, config)
url_for("webnotes.editor", "save_content", note_id="123abc")
# /app/notes/123abc/save_content
```  

### Changed

- Utilizzo di `mysql_api` nel modulo `db_strucrure`
- `utils.url_to_link` accetta il parametro opzionale `localdomains`. Se specificato
  deve essere una lista o una tupla di domini locali. Per i domini che non sono nella
  lista verrà aggiunto l'attibuto `target="blank"` al link in modo che il link venga
  aperto dal browser in una nuova *tab* o una nuova finestra.

### Fixed

- nel routing dei web service automatici (parametro `with_ws = True`)
  la regualar expression di validazione non accettava il punto (`.`) impedendo
  di fatto di utilizzare la sintassi "package.modulo" (Es: `wsgian.ws`)
  

