import logging
import os.path

from webob import Request
from webob import Response
from webob import exc
from webob import static

from . import driver
from .securecookie import SecureCookieSerializer
from . import utils
from . import cfg
from .url_mapping import url_for

__all__ = [
    "App",
    "LOG_LEVELS",
    "mtime_url",
    "quickstart",
]

try:
    unicode
except NameError:
    unicode = str

DAY = 86400

LOG_LEVELS = {
    "debug": logging.DEBUG,
    "info": logging.INFO,
    "warning": logging.WARNING,
    "error": logging.ERROR,
    "critical": logging.CRITICAL,
}


def mtime_url(pard, path):
    pard.setdefault("STATIC_DIR", None)
    if pard["STATIC_DIR"]:
        static_dir = pard["STATIC_DIR"]
    else:
        static_dir = utils.sib_path(__name__, "static")
    fn = f"{static_dir}/{path}"
    try:
        mt = str(os.path.getmtime(fn))[-4:]
        path = f"/static/{path}?mt={mt}"
    except FileNotFoundError:
        logging.error(f"FileNotFoundError: {fn}")
        path = f"/static/{path}"
    return path


class App:
    def __init__(self, urls, config={}):
        config = cfg.menage_alternatives(config)
        url_for.set_config(config)
        config.setdefault("with_static", False)
        if config["with_static"]:
            url_for.add_route({"pattern": "/static/{file_path:.*}", "module": "static"})
            config.setdefault("STATIC_DIR", None)
            if config["STATIC_DIR"]:
                self.static_dir = config["STATIC_DIR"]
            else:
                self.static_dir = utils.sib_path(__name__, "static")
            if os.path.isdir(self.static_dir):
                self.static_file_app = static.DirectoryApp(
                    self.static_dir, index_page=None
                )
            else:
                self.static_file_app = None
        for el in urls:
            url_for.add_route(el)
        config.setdefault("with_ws", False)
        if config["with_ws"]:
            url_for.add_route(
                {
                    "pattern": r"/api/{ws_module:[\w\.]+}/{ws_action:\w+}",
                    "module": "wsgian.ws",
                    "login": config.get("ws_auth", True),
                }
            )
        config.setdefault("CGI_ESCAPE", True)
        self.config = config
        ## Logging configuration
        level = LOG_LEVELS.get(config["LOGLEVEL"], logging.NOTSET)
        logging.basicConfig(level=level)

    def __call__(self, environ, start_response):
        self.req = Request(environ)
        self.req.charset = "utf8"
        controller = url_for.get_controller(self.req.path_info)
        if not controller:
            return exc.HTTPNotFound()(environ, start_response)
        elif controller["module"] == "static":
            self.req.path_info = controller["file_path"]
            return self.static_file_app(environ, start_response)
        else:
            self.resp = Response()
            self.exec_driver(controller)
            # logging.info(self.resp.headerlist)
            return self.resp(environ, start_response)

    def exec_driver(self, controller):
        # L'ordine di esecuzione e' importante per non sovrascrivere le chiavi
        # di headers e config
        params = self.get_params()  # Parametri POST e GET
        pard = {}
        pard.update(params)
        pard["request_parmeters"] = params
        pard.update(self.get_cookies())  # Cookies
        pard.update(self.get_headers())  # Header
        pard.update(self.config)  # Configurazione dell'app
        pard.update(controller)  # per ultimo carico il controller
        # che contiene le chiavi del modulo
        # da eseguire
        ## ToDo Mettere in try except con logging
        # import pprint
        # logging.info(pprint.pformat(pard))
        result = driver.driver(pard)
        if "redirect" in result:
            self.redirect(result["redirect"])
        if "delete_cookie" in result and isinstance(result["delete_cookie"], list):
            for k in result["delete_cookie"]:
                self.resp.delete_cookie(k)
                logging.info("Delete cookie: %s" % k)
        if "set_cookie" in result and isinstance(result["set_cookie"], dict):
            for k in result["set_cookie"]:
                if "set_cookie_expires_days" in result:
                    expires_days = result["set_cookie_expires_days"]
                else:
                    expires_days = None
                self.set_secure_cookie(k, result["set_cookie"][k], expires_days)
                logging.info("Set cookie `{}`: {}".format(k, result["set_cookie"][k]))
        if "redirect" in result:
            return
        if "serve_static" in result:
            self.resp = self.make_fileapp(result["serve_static"])
            return
        if "status" in result and isinstance(result["status"], int):
            self.resp.status = result["status"]
        if "header" in result and isinstance(result["header"], list):
            # self.resp.headerlist = result['header']
            self.set_headers(result["header"])
        else:
            # self.resp.headerlist = [('Content-type', 'text/html')]
            self.set_headers([("Content-type", "text/html")])
        if "html" in result:
            if isinstance(result["html"], unicode):
                self.resp.charset = "utf8"
                self.resp.text = result["html"]
                self.resp.content_length = len(result["html"].encode("utf8"))
            else:
                self.resp.body = result["html"]
                self.resp.content_length = len(result["html"])
        return

    def make_fileapp(self, path):
        return static.FileApp(path)

    def get_headers(self):
        d = {}
        for k in list(self.req.headers.keys()):
            http_key = "HTTP_%s" % k.upper().replace("-", "_")
            d[http_key] = self.req.headers[k]
        d["REMOTE_ADDR"] = self.req.remote_addr
        if d["REMOTE_ADDR"] == "localhost":
            d["REMOTE_ADDR"] = "127.0.0.1"
        d["HTTP_URL"] = self.req.url
        d["HTTP_PATH"] = self.req.path
        d["HTTP_QUERY_SRTING"] = self.req.query_string
        return d

    def set_headers(self, header_list):
        for el in header_list:
            key, value = el
            self.resp.headers[key] = value

    def get_params(self):
        d = {}
        for k, v in list(self.req.params.items()):
            d[k] = self.req.params.getall(k)
            if isinstance(d[k], list) and len(d[k]) == 1:
                d[k] = d[k][0]
            if self.config["CGI_ESCAPE"]:
                if "_noesc_" in k or k == "file_upload":
                    logging.debug(f"{k} not escaped!")
                else:
                    d[k] = utils.cgi_escape(d[k])
            if k == "file_upload":
                if hasattr(d[k], "filename") and hasattr(d[k], "file"):
                    filename = d[k].filename
                    content = d[k].file.read()
                    d[k] = {"filename": filename, "content": content, "form_id": k}
        return d

    def get_cookies(self):
        d = {}
        for k in list(self.req.cookies.keys()):
            cookie_value = self.req.cookies.get(k)
            parts = cookie_value.split("|")
            if len(parts) != 3:
                c_key = "simple_cookie_%s" % k
                d[c_key] = utils.cgi_escape(cookie_value)
            else:
                c_key = "cookie_%s" % k
                d[c_key] = self.get_secure_cookie(k)
            # d[c_key] = utils.cgi_escape(d[c_key])
        return d

    def redirect(self, location):
        self.resp = exc.HTTPSeeOther(location=location)

    def set_secure_cookie(self, name, value, expires_days=None):
        secure_cookie = SecureCookieSerializer(self.config["cookie_secret"])
        ck = secure_cookie.serialize(name, value)
        if expires_days:
            max_age = expires_days * DAY
        else:
            max_age = None
        self.resp.set_cookie(name, ck, max_age=max_age)

    def get_secure_cookie(self, name, expires_days=None):
        cookie_value = self.req.cookies.get(name)
        if cookie_value:
            if expires_days:
                max_age = int(expires_days * DAY)
            else:
                max_age = None
            secure_cookie = SecureCookieSerializer(self.config["cookie_secret"])
            return secure_cookie.deserialize(name, cookie_value, max_age)
        else:
            return None


# try:
# 	static_file_app = static.DirectoryApp(
# 		utils.sib_path(__name__ , 'static'), index_page=None)
# except IOError:
# 	static_file_app = None


def quickstart(app, serveraddr="127.0.0.1", port=8080):
    from wsgiref.simple_server import make_server, WSGIServer
    from socketserver import ThreadingMixIn

    class ThreadingWSGIServer(ThreadingMixIn, WSGIServer):
        pass

    server = make_server(serveraddr, port, app, ThreadingWSGIServer)
    print("Server Starts - {}:{}".format(serveraddr, port))
    try:
        server.serve_forever()
    except KeyboardInterrupt:
        print("Server Exits")
