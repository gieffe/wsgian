from . import account_db

import wsgian.json
import wsgian.mail
from wsgian import template
import wsgian.utils


def main(pard):
    pard.setdefault("action", "")

    if pard["action"] == "create":
        result = create_account(pard)
        pard["html"] = wsgian.utils.dump(result)
        return pard

    elif pard["action"] == "get":
        pard.setdefault("userid", "???")
        result = account_db.get(pard, pard["userid"])
        pard["header"] = [("Content-type", "application/json; charset=utf-8")]
        pard["html"] = wsgian.json.encode_date_to_iso(result)
        return pard

    elif pard["action"] == "check_login":
        result = check_login(pard)
        pard["html"] = wsgian.utils.dump(result)
        return pard

    elif pard["action"] == "get_list":
        args = {}
        args["limit"] = pard.get("limit", 20)
        args["offset"] = pard.get("offset", 0)
        result = account_db.get_list(pard, args)
        pard["html"] = wsgian.utils.dump(result)
        return pard

    else:
        pard["status"] = 400
        pard["html"] = "Wrong Action"
    return pard


def create_account(pard):
    result = {"account_creation": {}, "send_confirmation_email": {}}
    args = {}
    args["email"] = pard.get("email", "")
    args["password"] = pard.get("password", "")
    args["fullname"] = pard.get("fullname", "")

    r = account_db.create(pard, args)
    result["account_creation"] = r
    if not r["errors"]:
        r = send_account_confirmation(pard, r["userid"])
        result["send_confirmation_email"] = r

    return result


def check_login(pard):
    args = {}
    args["email"] = pard.get("email", "")
    args["password"] = pard.get("password", "")

    result = account_db.check_login(pard, args)
    return result
