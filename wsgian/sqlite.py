import itertools
import sqlite3
from sqlite3 import IntegrityError

from decimal import Decimal
from . import cfg

try:
    unicode
except NameError:
    unicode = str


def connect():
    """
    Utility method that get config parameters, connect to sqlite
    and return Connection object
    """
    pard = cfg.default_configuration()
    pard = cfg.get_dot_env_config(pard, extra_params=[])
    pard = cfg.menage_alternatives(pard)
    pard.setdefault("db.sqlite", {"db_path": None})
    if not pard["db.sqlite"]["db_path"]:
        pard["db.sqlite"]["db_path"] = "test.sqlite"
    return DB(**pard["db.sqlite"])


class DB:
    def __init__(self, db_path, db_structure={}):
        self.db = sqlite3.connect(db_path)
        self.charset = "utf8"
        self.db_structure = db_structure

    def query(self, query, parameters=()):
        cursor = self.db.cursor()
        try:
            cursor.execute(query, parameters)
            column_names = [d[0] for d in cursor.description]
            return [dict(zip(column_names, row)) for row in cursor]
        finally:
            cursor.close()

    def execute(self, query, parameters=()):
        cursor = self.db.cursor()
        try:
            cursor.execute(query, parameters)
            return cursor.lastrowid
        finally:
            cursor.close()

    def commit(self):
        self.db.commit()

    def rollback(self):
        self.db.rollback()

    def get_table_list(self):
        sql = """
            SELECT name FROM sqlite_master
            WHERE type='table'
            ORDER BY name
            """
        result = self.query(sql)
        return [rec["name"] for rec in result]

    def test_database(self, drop=False):
        ll = self.get_table_list()
        for table in self.db_structure:
            if drop:
                sql = "drop table if exists %s" % table
                self.execute(sql)
            if table not in ll or drop:
                self.execute(self.db_structure[table])
                print("Create Table %s" % table)

    def escape_string(self, s):
        if isinstance(s, unicode):
            res = self._escape(s.encode(self.charset))
        else:
            res = self._escape(s)
        # logging.info('escaping %s >>%s<<' % (repr(s), repr(res)))
        return res

    def _escape(self, value):
        """
        Escapes special characters as they are expected to by when MySQL
        receives them.
        As found in MySQL source mysys/charset.c

        Returns the value if not a string, or the escaped string.
        """
        if value is None:
            return value
        elif isinstance(value, (int, float, Decimal)):
            return value
        res = value
        res = res.replace("\\", "\\\\")
        res = res.replace("\n", "\\n")
        res = res.replace("\r", "\\r")
        res = res.replace("\047", "\134\047")  # single quotes
        res = res.replace("\042", "\134\042")  # double quotes
        res = res.replace("\032", "\134\032")  # for Win32
        return res

    def close(self):
        """Closes this database connection."""
        if getattr(self, "db", None) is not None:
            self.db.close()
            self.db = None

    def __del__(self):
        self.close()
