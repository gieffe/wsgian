import logging
from .param import Param
from . import mysql_api


class AlterLog(Param):
    table_name = "alter_log"


def check(db_tables=None, db_alters=None):

    ## Tables
    if db_tables and isinstance(db_tables, dict):
        if db_alters:
            db_tables.update([AlterLog.create_statement()])
        sql = "show tables"
        result = mysql_api.select(sql)
        table_list = [list(rec.values())[0] for rec in result]
        for table_name in db_tables:
            if table_name not in table_list:
                sql = db_tables[table_name]
                mysql_api.execute(sql)
                logging.info(f"create table {table_name}")

    ## Alter table
    if db_alters and isinstance(db_alters, list):
        alter_log = AlterLog("alter_stm")
        executed = alter_log.dict()

        for rec in db_alters:
            if rec["id"] not in executed:
                logging.info(f"Exec statement: {rec['stm']} ...")
                mysql_api.execute(rec["stm"])
                logging.info("done")
                logging.info("")
                if rec["id"] != "0":
                    alter_log.set(rec["id"], rec["stm"])


def _test():
    import pprint

    logging.basicConfig(level=logging.INFO)

    db_tables = {
        "pippo": """
            CREATE TABLE `pippo` (
              `name` varchar(40) NOT NULL,
              `key` varchar(40) NOT NULL,
              `value` text,
              `ind` smallint unsigned NOT NULL DEFAULT '0',
              PRIMARY KEY (`name`,`key`)
            )
            """,
    }
    db_alters = [
        {
            "id": "test-1",
            "stm": "alter table pippo add column upd_ts timestamp",
        },
        {
            "id": "test-2",
            "stm": "alter table pippo add column upd_user varchar(50) not null default ''",
        },
    ]
    check(db_tables, db_alters)

    alter_log = AlterLog("alter_stm")
    pprint.pprint(alter_log.list())


if __name__ == "__main__":
    _test()
