from . import account_db
import os
import urllib.request, urllib.parse, urllib.error

from wsgian import template
import wsgian.utils
import wsgian.json
import wsgian.mail

from .account_db import get_user


def auth_urls(pard):
    """
    Configure auth pattern

    input
            'LOGIN_URL': '/login',
            'LOGOUT_URL': '/logout',
            'SIGNUP_URL': '/signup',

    """
    urls = [
        # 	{'pattern': '/1/acct/create',  'module': 'account', 'action': 'create'},
        # 	{'pattern': '/1/acct/get/{userid:\w+}',  'module': 'account', 'action': 'get'},
        # 	{'pattern': '/1/acct/check',  'module': 'account', 'action': 'check_login'},
        # 	{'pattern': '/1/acct/list',  'module': 'account', 'action': 'get_list'},
        {"pattern": pard["SIGNUP_URL"], "module": "wsgian.gauth", "action": "signup"},
        {
            "pattern": pard["SIGNUP_URL"] + "/create_account",
            "module": "wsgian.gauth",
            "action": "create_account",
        },
        {"pattern": pard["LOGIN_URL"], "module": "wsgian.gauth", "action": "login"},
        {
            "pattern": pard["LOGIN_URL"] + "/authenticate",
            "module": "wsgian.gauth",
            "action": "authenticate",
        },
        {
            "pattern": pard["LOGIN_URL"] + r"/google_auth/{token:[\w\.\-_]+}",
            "module": "wsgian.gauth",
            "action": "google_auth",
        },
        {
            "pattern": pard["LOGIN_URL"] + "/forgot",
            "module": "wsgian.gauth",
            "action": "forgot",
        },
        {
            "pattern": pard["LOGIN_URL"] + r"/cancel_reset/{token:\w+}",
            "module": "wsgian.gauth",
            "action": "cancel_reset",
        },
        {"pattern": pard["LOGOUT_URL"], "module": "wsgian.gauth", "action": "logout"},
        {
            "pattern": get_account_setting_url(),
            "module": "wsgian.gauth",
            "action": "settings",
            "login": True,
        },
        {
            "pattern": r"%s/{token:\w+}" % get_account_confirm_url(),
            "module": "wsgian.gauth",
            "action": "confirm",
            "login": True,
        },
        {
            "pattern": r"%s/{token:\w+}" % get_account_reject_url(),
            "module": "wsgian.gauth",
            "action": "reject",
            "login": True,
        },
        {
            "pattern": r"%s/{token:\w+}" % get_account_confirm_invitation_url(),
            "module": "wsgian.gauth",
            "action": "invitation",
        },
        {
            "pattern": r"/gauth/acct/send_confirm_request/{userid:\w+}",
            "module": "wsgian.gauth",
            "action": "send_confirm_request",
        },
        {
            "pattern": "/gauth/acct/change_password",
            "module": "wsgian.gauth",
            "action": "change_password",
            "login": True,
        },
        {
            "pattern": "/gauth/acct/change_password/confirm",
            "module": "wsgian.gauth",
            "action": "change_password_confirm",
            "login": True,
        },
    ]

    return urls


def get_account_setting_url():
    return "/gauth/acct/settings"


def get_account_confirm_url():
    return "/gauth/acct/confirm"


def get_account_reject_url():
    return "/gauth/acct/reject"


def get_account_confirm_invitation_url():
    return "/gauth/acct/invitation"


def main(pard):
    pard.setdefault("action", "")

    if pard["action"] == "signup":
        args = {}
        args["fullname"] = ""
        args["email"] = ""
        args["password"] = ""
        args["errors"] = []
        pard["main_body"] = render_signup(pard, args)
        return render_page(pard)

    elif pard["action"] == "create_account":
        args = {}
        args["fullname"] = pard.get("user_name", "")
        args["email"] = pard.get("email", "")
        args["password"] = pard.get("password", "")
        result = account_db.create(pard, args)
        if result["errors"]:
            args["errors"] = result["errors"]
            pard["main_body"] = render_signup(pard, args)
            return render_page(pard)
        else:
            r = send_account_confirmation(pard, result["userid"])
            print(wsgian.utils.dump(r))
            pard["set_cookie"] = {"sid_user": result["userid"]}
            pard["redirect"] = "/"
            return pard

    elif pard["action"] == "login":
        pard.setdefault("continue", "/")
        pard["main_body"] = render_login(pard)
        return render_page(pard)

    elif pard["action"] == "logout":
        pard["delete_cookie"] = ["sid_user"]
        pard["redirect"] = "/"
        return pard

    elif pard["action"] in ("authenticate", "google_auth"):
        pard.setdefault("continue", "/")
        if pard["action"] == "google_auth":
            result = check_google_auth(pard)
        else:
            result = check_login(pard)
        if not result["errors"]:
            if "remember_me" in pard and pard["remember_me"]:
                pard["set_cookie_expires_days"] = 30
            pard["set_cookie"] = {"sid_user": result["act_data"]["userid"]}
            pard["redirect"] = urllib.parse.unquote_plus(pard["continue"])
            return pard
        else:
            pard["main_body"] = render_login(pard, result["errors"])
            return render_page(pard)

    elif pard["action"] == "settings":
        pard["main_body"] = wsgian.utils.dump(pard["user_data"])
        return render_page(pard)

    elif pard["action"] == "confirm_account":
        pard["main_body"] = render_confirm_acount(pard)
        return render_page(pard)

    elif pard["action"] == "send_confirm_request":
        r = send_account_confirmation(pard, pard["userid"])
        alert = ""
        msg = ""
        if "errors" in r and r["errors"]:
            alert = (
                '<div class="alert alert-danger" role="alert">%s</div>' % r["errors"][0]
            )
        else:
            msg = (
                '<div class="alert alert-success" role="alert">we\'ve emailed you a'
                " confirmation request</div>"
            )
        pard["main_body"] = (
            """
			   %(alert)s
			   %(msg)s
			   <a class="btn btn-primary" href="/" role="button">Continue</a>
			"""
            % {"alert": alert, "msg": msg}
        )
        # pard['header'] = [('Content-type', 'application/json; charset=utf-8')]
        # pard['html'] = wsgian.json.encode_date_to_iso(r)
        # return pard
        return render_page(pard)

    elif pard["action"] in ("confirm", "reject"):
        r = account_db.confirm_account(
            pard, {"action": pard["action"], "token": pard["token"]}
        )
        pard["main_body"] = render_confirm(pard, r)
        return render_page(pard)

    elif pard["action"] == "invitation":
        args = {}
        new_user = account_db.get_by_token(pard, pard["token"])
        if not new_user:
            pard["main_body"] = "Expired token"
            return render_page(pard)
        else:
            pard["set_cookie"] = {"sid_user": new_user["userid"]}
            pard["redirect"] = "/gauth/acct/change_password"
            return pard

    elif pard["action"] == "change_password":
        args = {}
        args["fullname"] = pard["user_data"]["fullname"]
        args["email"] = pard["user_data"]["email"]
        args["errors"] = []
        pard["main_body"] = render_change_password(pard, args)
        return render_page(pard)

    elif pard["action"] == "change_password_confirm":
        args = {}
        args["fullname"] = pard.get("user_name", "")
        args["email"] = pard["user_data"]["email"]
        args["new_password"] = pard.get("new_password", "")
        args["new_password_again"] = pard.get("new_password_again", "")
        args["token"] = pard["user_data"]["token"]
        result = account_db.change_password_with_token(pard, args)
        if result["errors"]:
            args["errors"] = result["errors"]
            pard["main_body"] = render_change_password(pard, args)
            return render_page(pard)
        else:
            pard["redirect"] = "/"
            return pard

    elif pard["action"] == "forgot":
        user_data = account_db.get_by_email(pard, pard["email"])
        if not user_data:
            pard["main_body"] = render_login(
                pard, ["The email you entered is incorrect."]
            )
            return render_page(pard)
        elif user_data["provider"] == "google":
            pard["main_body"] = render_login(pard, ["This is a google account"])
            return render_page(pard)
        else:
            result = send_reset_email(pard, user_data)
            if result == "Success":
                resd = {
                    "msg": [
                        "Reset your %(APPLICATION_NAME)s password. Help is on the way."
                        % pard
                    ]
                }
            else:
                resd = {"errors": [result]}
            pard["main_body"] = render_confirm(pard, resd)
            return render_page(pard)

    elif pard["action"] == "cancel_reset":
        r = account_db.reset_token(pard, pard["token"])
        pard["main_body"] = render_confirm(pard, {"msg": ["Password Reset Cancelled"]})
        return render_page(pard)

    else:
        pard["status"] = 400
        pard["html"] = "Wrong Action"

    return pard


def send_reset_email(pard, user):
    d = {}
    d["fullname"] = user["fullname"]
    d["APPLICATION_NAME"] = pard["APPLICATION_NAME"]
    if pard["APPSERVER"] == "/":
        d["APPSERVER"] = pard["HTTP_PROTOCOL"] + "://" + pard["HTTP_HOST"]
    else:
        d["APPSERVER"] = pard["APPSERVER"]
    d["ACT_CONF_INVITATION_URL"] = get_account_confirm_invitation_url()
    d["token"] = account_db.new_token(pard, user["userid"])
    d["cancel_reset_url"] = pard["LOGIN_URL"] + "/cancel_reset"
    user.setdefault("lang", "en")
    if user["lang"] not in pard["LANGUAGES"]:
        user["lang"] = "en"
    subject = template.render(
        "email_reset_subject_%s.txt" % user["lang"], d, tdir=get_tdir()
    )
    body = template.render("email_reset_body_%s.txt" % user["lang"], d, tdir=get_tdir())
    try:
        res_email = wsgian.mail.send(
            pard,
            {
                "from": pard["EMAIL"]["default_sender"],
                "to": [{"name": user["fullname"], "email": user["email"]}],
                "subject": subject,
                "content": body,
            },
        )
    # 		message = wsgian.mail.EmailMessage(
    # 			sender = pard['DEFAULT_SENDER'],
    # 			to = '<%s>' % user['email'],
    # 			subject=subject,
    # 			body=body)
    # 		message.send(pard)
    except Exception as err:
        logging.error(str(err))
        return "Reset error for %(email)s" % user
    return "Success"


def js_login(pard):
    h = (
        """
		<script>
			$("#link-forgot").on("click", forgot_password);
			function forgot_password(e) {
			e.preventDefault();
			var value = $.trim($('#inputEmail').val());
			if (value != '') {
				$('#signinForm').attr('action', "%(LOGIN_URL)s/forgot").submit();
				}
			else {
				alert('Please, fill Email Address');
				}
			}
		</script>
		"""
        % pard
    )
    return h


def render_login(pard, errors=[]):
    pard["email"] = pard.get("email", "")
    pard["javascript"] = js_login(pard)
    if errors:
        tmp = []
        for err in errors:
            tmp.append('<div class="alert alert-danger" role="alert">%s</div>' % err)
        pard["alert"] = "\n".join(tmp)
    else:
        pard["alert"] = ""

    ## Google Logout
    #
    # 	<a href="#" onclick="signOut();">Sign out</a>
    # 	<script>
    # 	  function signOut() {
    # 		var auth2 = gapi.auth2.getAuthInstance();
    # 		auth2.signOut().then(function () {
    # 		  console.log('User signed out.');
    # 		});
    # 	  }
    # 	</script>
    #

    if "GOOGLE" in pard and pard["GOOGLE"]:
        pard[
            "google_auth_login"
        ] = """		
			<a href="#" id="GoogleBtn" class="btn btn-lg btn-danger btn-block">
			<i class="fa fa-google"></i> Log in with <b>Google</b>
			</a>			
			<script>build_google_login();</script>
			"""
    else:
        pard["google_auth_login"] = ""

    h = (
        """	   
	   <form class="form-signin" action="%(LOGIN_URL)s/authenticate" id="signinForm" method="post">
	   %(alert)s
	    <input type="hidden" name="continue" value="%(continue)s">
        <h2 class="form-signin-heading">Log in to %(PROJECT_TITLE)s</h2>
        <label for="inputEmail" class="sr-only">Email address</label>
        <input type="email" id="inputEmail" name="email" class="form-control" placeholder="Email address" value="%(email)s" required autofocus>
        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password" id="inputPassword" name="password" class="form-control" placeholder="Password">
        <div class="checkbox">
          <label>
            <input type="checkbox" name="remember_me" value="remember-me" checked> Remember me
          </label>
        </div>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Log in</button>
        
        %(google_auth_login)s
        
        <button class="btn btn-link" type="button" id="link-forgot">Forgot your password?</button>
      </form>
	"""
        % pard
    )
    return h


def render_confirm_acount(pard):
    h = """
	   <div class="form-signin">
        <h2 class="form-signin-heading">{PROJECT_TITLE}</h2>
        <div class="alert alert-warning" role="alert">Please confirm your email address <strong>{email}</strong></div>
        <a class="btn btn-primary" href="/gauth/acct/send_confirm_request/{userid}" role="button">Resend email</a>
      </div>
	""".format(
        PROJECT_TITLE=pard["PROJECT_TITLE"],
        email=pard["user_data"]["email"],
        userid=pard["user_data"]["userid"],
    )
    return h


def render_signup(pard, args):
    _args = args.copy()
    _args["PROJECT_TITLE"] = pard["PROJECT_TITLE"]
    _args["SIGNUP_URL"] = pard["SIGNUP_URL"]
    if "errors" in _args and _args["errors"]:
        tmp = []
        for err in _args["errors"]:
            tmp.append('<div class="alert alert-danger" role="alert">%s</div>' % err)
        _args["alert"] = "\n".join(tmp)
    else:
        _args["alert"] = ""

    if "GOOGLE" in pard and pard["GOOGLE"]:
        _args[
            "google_auth_signin"
        ] = """		
			<a href="#" id="GoogleBtn" class="btn btn-lg btn-danger btn-block">
			<i class="fa fa-google"></i> Sign up with <b>Google</b>
			</a>			
			<script>build_google_login();</script>
			"""
    else:
        _args["google_auth_signin"] = ""

    h = (
        """
	   <form class="form-signin" action="%(SIGNUP_URL)s/create_account" method="post" id="signinForm">
	   %(alert)s
        <h2 class="form-signin-heading">Create a %(PROJECT_TITLE)s account</h2>
		<input type="hidden" name="action" value="create_account">
        <label for="inputName" class="sr-only">Name</label>
        <input type="text" id="inputName" name="user_name" class="form-control" placeholder="Name: e.g., Niroo Seitubi" value="%(fullname)s" required autofocus>
        <label for="inputEmail" class="sr-only">Email address</label>
        <input type="email" id="inputEmail" name="email" class="form-control" placeholder="Email address" value="%(email)s" required>
        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password" id="inputPassword" name="password" class="form-control" placeholder="Password" required>
        <br />
        <button class="btn btn-lg btn-success btn-block" type="submit">Create New Account</button>
        %(google_auth_signin)s
      </form>
	"""
        % _args
    )
    return h


def render_change_password(pard, args):
    _args = args.copy()
    _args["APPLICATION_NAME"] = pard["APPLICATION_NAME"]
    if "errors" in _args and _args["errors"]:
        tmp = []
        for err in _args["errors"]:
            tmp.append('<div class="alert alert-danger" role="alert">%s</div>' % err)
        _args["alert"] = "\n".join(tmp)
    else:
        _args["alert"] = ""
    h = (
        """
	   <form class="form-signin" action="/gauth/acct/change_password/confirm" method="post">
	   %(alert)s
        <h2 class="form-signin-heading">Confirm %(APPLICATION_NAME)s account</h2>
		<input type="hidden" name="action" value="change_password">
        <label for="inputName" class="sr-only">Name</label>
        <input type="text" id="inputName" name="user_name" class="form-control" placeholder="Name" value="%(fullname)s" required autofocus>
        <label for="inputEmail" class="sr-only">Email address</label>
        <input type="email" id="inputEmail" name="email" class="form-control" placeholder="Email address" value="%(email)s" required disabled>
        <label for="inputPassword" class="sr-only">New Password</label>
        <input type="password" id="inputPassword" name="new_password" class="form-control" placeholder="New Password" required>
        <label for="inputPassword2" class="sr-only">Retype Password</label>
        <input type="password" id="inputPassword2" name="new_password_again" class="form-control" placeholder="Retype Password" required>
        <br />
        <button class="btn btn-lg btn-success btn-block" type="submit">Confirm</button>
      </form>
	"""
        % _args
    )
    return h


def render_confirm(pard, resd):
    errors = resd["errors"] if "errors" in resd else []
    if errors:
        tmp = []
        for err in errors:
            tmp.append('<div class="alert alert-danger" role="alert">%s</div>' % err)
        pard["alert"] = "\n".join(tmp)
    else:
        pard["alert"] = ""

    msgs = resd["msg"] if "msg" in resd else ""
    if msgs:
        tmp = []
        for msg in msgs:
            tmp.append('<div class="alert alert-success" role="alert">%s</div>' % msg)
        pard["msg"] = "\n".join(tmp)
    else:
        pard["msg"] = ""

    h = (
        """
	   %(alert)s
	   %(msg)s
       <a class="btn btn-primary" href="/" role="button">Continue</a>
	"""
        % pard
    )
    return h


def google_auth_head(pard):
    pard["client_id"] = pard["GOOGLE"]["client_id"]
    h = (
        """
		<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet" type="text/css">
		<script src="https://apis.google.com/js/api:client.js"></script>
		<script>
		var googleUser = {};
		var build_google_login = function() {
			gapi.load('auth2', function(){
			  // Retrieve the singleton for the GoogleAuth library and set up the client.
			  auth2 = gapi.auth2.init({
				client_id: '%(client_id)s',
				cookiepolicy: 'single_host_origin',
				// Request scopes in addition to 'profile' and 'email'
				//scope: 'additional_scope'
			  });
			  attachSignin(document.getElementById('GoogleBtn'));
			});
			};

		function attachSignin(element) {
			console.log(element.id);
			auth2.attachClickHandler(element, {},
			
				// onSuccess
				function(googleUser) {
				  //document.getElementById('name').innerText = "Signed in: " +
				  //	  googleUser.getBasicProfile().getName();
				  var id_token = googleUser.getAuthResponse().id_token;
				  $('#signinForm').attr('action', "%(LOGIN_URL)s/google_auth/" + id_token).submit();  
				},
			
				// onFailure
				function(error) {
				  alert(JSON.stringify(error, undefined, 2));
				});
			}
		</script>			
		"""
        % pard
    )
    return h


def render_page(pard):
    pard.setdefault("TITLE", "")
    pard.setdefault("PROJECT", "gauth")
    pard.setdefault("PROJECT_TITLE", "GAuth")
    pard.setdefault("PROJECT_SUBTITLE", "Gian's basic authentication project")
    pard.setdefault("main_body", "")
    pard.setdefault("javascript", "")
    pard.setdefault("TEMPLATE_RELOAD", True)
    pard.setdefault("google_auth_head", "")
    if "GOOGLE" in pard and pard["GOOGLE"]:
        pard["google_auth_head"] = google_auth_head(pard)
    pard["html"] = template.render(
        "login.html", pard, tdir=get_tdir(), force_reload=pard["TEMPLATE_RELOAD"]
    )
    return pard


def get_tdir():
    this_dir, this_filename = os.path.split(__file__)
    tdir = os.path.join(this_dir, "template")
    return tdir


# -------------------------------------------------------------------------- #


def check_login(pard):
    args = {}
    args["email"] = pard.get("email", "")
    args["password"] = pard.get("password", "")

    result = account_db.check_login(pard, args)
    return result


def check_google_auth(pard):
    """
    {u'at_hash': u'aKqk2R4xwVJOXLT8-V8z7w',
     u'aud': u'...gsa.apps.googleusercontent.com',
     u'azp': u'...apps.googleusercontent.com',
     u'email': u'quel@gmail.com',
     u'email_verified': True,
     u'exp': 1553078457,
     u'family_name': u'Plutarsky',
     u'given_name': u'Pluto',
     u'iat': 1553074857,
     u'iss': u'accounts.google.com',
     u'jti': u'...',
     u'locale': u'en-GB',
     u'name': u'Pluto Plutarsky',
     u'picture': u'',
     u'sub': u'user-id'}
    """
    from google.oauth2 import id_token
    from google.auth.transport import requests

    result = {"errors": []}
    token = pard["token"]
    CLIENT_ID = pard["GOOGLE"]["client_id"]
    try:
        idinfo = id_token.verify_oauth2_token(token, requests.Request(), CLIENT_ID)
        if idinfo["iss"] not in ["accounts.google.com", "https://accounts.google.com"]:
            raise ValueError("Wrong issuer.")
        if not idinfo["email_verified"]:
            raise ValueError("Unverified Google email can't be used")

        userid = idinfo["sub"]
        act_data = account_db.get_user(pard, userid)
        if not act_data:
            args = {}
            args["userid"] = userid
            args["fullname"] = idinfo["name"]
            args["email"] = idinfo["email"]
            args["provider"] = "google"
            args["status"] = "active"
            resd = account_db.create(pard, args)
            if resd["errors"]:
                result["errors"] = resd["errors"]
                return result
            else:
                act_data = account_db.get_user(pard, userid)
        result["act_data"] = act_data

    except ValueError as err:
        result["errors"] = err

    return result


def send_account_confirmation(pard, userid):
    """Send email to account for confirmation

    Args:
            userid
            pard['APPLICATION_NAME'],
            pard['APPSERVER']
            pard['ACT_CONF_URL'],
            pard['ACT_REJECT_URL'],
            pard['DEFAULT_SENDER'],
            pard['SMTP_HOST'] default 'localhost'
            pard['SMTP_PORT'] default ''
            pard['SMTP_USER'] default ''
            pard['SMTP_PASSW'] default ''

    Returns:
            {'email_message': '', 'errors': []}

    """
    resd = {"email_message": "", "errors": []}

    ## Check account existences
    act = account_db.get(pard, userid)
    if not act:
        resd["errors"] = ["I can't find this account: %s" % userid]
        return resd

    ## Check app configuration
    conf_keys = (
        "APPLICATION_NAME",
        "APPSERVER",
        #'DEFAULT_SENDER',
        "LANGUAGES",
    )

    for k in conf_keys:
        if k not in pard:
            resd["errors"].append(
                "Email confirmation is not properly configured (missing %s)" % k
            )
    if resd["errors"]:
        return resd

    ## Set defaults
    pard.setdefault("SMTP_HOST", "localhost")
    pard.setdefault("SMTP_PORT", "")
    pard.setdefault("SMTP_USER", "")
    pard.setdefault("SMTP_PASSW", "")

    ## Get Token
    pard["token"] = account_db.new_token(pard, userid)

    ## Get user language
    act.setdefault("lang", "en")
    if act["lang"] not in pard["LANGUAGES"]:
        act["lang"] = "en"

    d = {}
    d["fullname"] = act["fullname"]
    d["ACT_CONF_URL"] = get_account_confirm_url()
    d["ACT_REJECT_URL"] = get_account_reject_url()
    d["APPLICATION_NAME"] = pard["APPLICATION_NAME"]
    d["token"] = pard["token"]
    if pard["APPSERVER"] == "/":
        d["APPSERVER"] = pard["HTTP_PROTOCOL"] + "://" + pard["HTTP_HOST"]
    else:
        d["APPSERVER"] = pard["APPSERVER"]

    pard.setdefault("TEMPLATE_RELOAD", True)
    ## Build message
    message_body = template.render(
        "email_confirmation_body_%s.txt" % act["lang"],
        d,
        tdir=get_tdir(),
        force_reload=pard["TEMPLATE_RELOAD"],
    )
    message_subject = template.render(
        "email_confirmation_subject_%s.txt" % act["lang"],
        d,
        tdir=get_tdir(),
        force_reload=pard["TEMPLATE_RELOAD"],
    )

    ## Send message
    try:
        res_email = wsgian.mail.send(
            pard,
            {
                "from": pard["EMAIL"]["default_sender"],
                "to": [{"name": act["fullname"], "email": act["email"]}],
                "subject": message_subject,
                "content": message_body,
            },
        )

    # 		message = wsgian.mail.EmailMessage(
    # 			sender = pard['DEFAULT_SENDER'],
    # 			to = '%s <%s>' % (act['fullname'], act['email']),
    # 			subject=message_subject,
    # 			body=message_body)
    #
    # 		message.send(pard)
    except Exception as err:
        resd["errors"] = ["Error sending email confirmation", str(err)]
        return resd

    resd["message_body"] = message_body

    return resd
