wsgian
======
wsgian is my *personal* wsgi micro-framework

### A basic *wsgian* web application ###

The following example starts a server and hosts an application
that will be served at request reaching http://127.0.0.1:8080/

```python
import wsgian

def main(pard):
    pard.setdefault('who', 'wsgian')
    return {'html': 'Hello %(who)s!' % pard}

urls = [
    {'pattern': '/{who:\w+}', 'module':  'hello'},
    {'pattern': '/',  'module': 'hello'},
    ]

config = {
    'with_static': True,
    'DEBUG': True,
    'LOGLEVEL': 'debug',
    'cookie_secret': 'substitutewithyourcookiesecret',
    }

app = wsgian.App(urls, config)

if __name__ == '__main__':
    wsgian.quickstart(app)
```

Store this code snippet into a file named *hello.py* and execute it
as follows:

    python hello.py

### Basic authentication module ###
*wsgian* came with a basic authentication module called **gauth**.
[gauth_app]'s repository contain a bare app that uses *gauth* for authentication.

[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)

[gauth_app]: https://bitbucket.org/gieffe/gauth_app
