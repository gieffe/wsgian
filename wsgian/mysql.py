#!/usr/bin/env python
#
# Copyright 2009 Facebook
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

"""A lightweight wrapper around pymysql."""

import pymysql
import logging
import os
import time

from . import mysql_api

try:
    unicode
except NameError:
    unicode = str


# Alias
connect = mysql_api.connect


class Connection:
    """A lightweight wrapper around pymysql DB-API connections."""

    def __init__(
        self, host, database, user=None, password=None, max_idle_time=7 * 3600
    ):
        self.host = host
        self.database = database
        self.max_idle_time = max_idle_time

        args = dict(use_unicode=True, charset="utf8", db=database, autocommit=True)
        if user is None:
            if "MYSQL_USER" in os.environ:
                args["user"] = os.environ["MYSQL_USER"]
        else:
            args["user"] = user
        if password is None:
            if "MYSQL_PASSWORD" in os.environ:
                args["passwd"] = os.environ["MYSQL_PASSWORD"]
        else:
            args["passwd"] = password

        # We accept a path to a MySQL socket file or a host(:port) string
        if "/" in host:
            args["unix_socket"] = host
        else:
            self.socket = None
            pair = host.split(":")
            if len(pair) == 2:
                args["host"] = pair[0]
                args["port"] = int(pair[1])
            else:
                args["host"] = host
                args["port"] = 3306

        self._db = None
        self._db_args = args
        self._last_use_time = time.time()
        self.last_executed = ""
        try:
            self.reconnect()
        except:
            logging.error("Cannot connect to MySQL on %s", self.host, exc_info=True)

    def __del__(self):
        self.close()

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, exc_tb):
        self.close()

    def close(self):
        """Closes this database connection."""
        if getattr(self, "_db", None) is not None:
            try:
                self._db.close()
            except (OperationalError, MySQLError) as err:
                pass
            self._db = None

    def reconnect(self):
        """Closes the existing database connection and re-opens it."""
        self.close()
        self._db = mysql_api.connect(cnf=None, alt_db="", **self._db_args)
        logging.debug("connect to MySQL on %s" % self.host)

    def iter(self, query, parameters=None):
        """Returns an iterator for the given query and parameters."""
        self._ensure_connected()
        cursor = pymysql.cursors.SSCursor(self._db)
        try:
            self._execute(cursor, query, parameters)
            column_names = [d[0] for d in cursor.description]
            for row in cursor:
                yield dict(list(zip(column_names, row)))
        finally:
            self._get_last_executed(cursor)
            cursor.close()

    def query(self, query, parameters=None):
        """Returns a row list for the given query and parameters."""
        cursor = self._cursor()
        try:
            self._execute(cursor, query, parameters)
            column_names = [d[0] for d in cursor.description]
            return [dict(zip(column_names, row)) for row in cursor]
        finally:
            self._get_last_executed(cursor)
            cursor.close()

    def _get_last_executed(self, cursor):
        if hasattr(cursor, "_executed"):
            self.last_executed = cursor._executed
        elif hasattr(cursor, "_last_executed"):
            self.last_executed = cursor._last_executed
        elif hasattr(cursor, "statement"):
            self.last_executed = cursor.statement
        else:
            self.last_executed = "na"

    def send(self, query, parameters=None):
        """Returns a tuple list for the given query and parameters."""
        cursor = self._cursor()
        try:
            self._execute(cursor, query, parameters)
            return [row for row in cursor]
        finally:
            self._get_last_executed(cursor)
            cursor.close()

    def get(self, query, parameters=None):
        """Returns the first row returned for the given query."""
        rows = self.query(query, parameters)
        if not rows:
            return None
        elif len(rows) > 1:
            raise Exception("Multiple rows returned for Database.get() query")
        else:
            return rows[0]

    def execute(self, query, parameters=None):
        """Executes the given query, returning the lastrowid from the query."""
        cursor = self._cursor()
        try:
            self._execute(cursor, query, parameters)
            return cursor.lastrowid
        finally:
            self._get_last_executed(cursor)
            cursor.close()

    def executemany(self, query, parameters):
        """Executes the given query against all the given param sequences.

        We return the lastrowid from the query.
        """
        cursor = self._cursor()
        try:
            cursor.executemany(query, parameters)
            return cursor.lastrowid
        finally:
            self._get_last_executed(cursor)
            cursor.close()

    def _ensure_connected(self):
        # Mysql by default closes client connections that are idle for
        # 8 hours, but the client library does not report this fact until
        # you try to perform a query and it fails.	Protect against this
        # case by preemptively closing and reopening the connection
        # if it has been idle for too long (7 hours by default).
        if (
            not getattr(self._db, "open", True)
            or self._db is None
            or (time.time() - self._last_use_time > self.max_idle_time)
        ):
            self.reconnect()
        self._last_use_time = time.time()

    def _cursor(self):
        self._ensure_connected()
        return self._db.cursor()

    def _execute(self, cursor, query, parameters):
        try:
            return cursor.execute(query, parameters)
        except (OperationalError, MySQLError):
            logging.error("Error connecting to MySQL on %s", self.host)
            self.close()
            raise

    def escape_string(self, s):
        if isinstance(s, unicode):
            x = self._db.escape_string(s.encode(self._db_args["charset"]))
            return x.decode(self._db_args["charset"])
        else:
            return self._db.escape_string(s)


# Alias of common MySQL exceptions
IntegrityError = pymysql.IntegrityError
OperationalError = pymysql.OperationalError
MySQLError = pymysql.MySQLError
DatabaseError = pymysql.DatabaseError
