import logging
import os
import functools
import markupsafe
import time

from jinja2 import Environment, FileSystemLoader, select_autoescape, StrictUndefined
from jinja2_fragments import render_block, render_blocks
from . import utils
from . import cfg

TEMPLATES = {}

# class Template(object):
#
# 	def __init__(self):
# 		self.template_dict = {}
# 		self.template_dir = utils.sib_path(__name__, 'template')
# 		logging.warning('INIT %s' % self.template_dir)
#
# 	def load_template(self, template_name):
# 		fn = os.path.join(self.template_dir, template_name)
# 		html = open(fn).read()
# 		logging.warning('Load %s' % fn)
# 		self.template_dict[template_name] = html
#
# 	def render(self, template_name, pard):
# 		if template_name not in self.template_dict:
# 			self.load_template(template_name)
# 		html = self.template_dict[template_name] % pard
# 		return html

# template = Template()


def render(template_name, pard, tdir=None, force_reload=False):
    if template_name not in TEMPLATES or force_reload:
        if not tdir:
            tdir = utils.sib_path(__name__, "template")
        fn = os.path.join(tdir, template_name)
        html = open(fn).read()
        logging.info("Load %s" % fn)
        TEMPLATES[template_name] = html
    return TEMPLATES[template_name] % pard


def smart_date(ts):
    now = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
    t = time.strptime(ts[0:10], "%Y-%m-%d")
    if ts[:4] == now[:4]:
        tt = time.strftime("%d %b", t)
    else:
        tt = time.strftime("%d %b %Y", t)
    return tt


def rev_date(dt):
    ll = dt.split("-")
    ll.reverse()
    return "-".join(ll)


def dump(s):
    r = utils.dump(s)
    return markupsafe.Markup(r)


filters = {
    "smart_date": smart_date,
    "rev_date": rev_date,
    "dump": dump,
}


@functools.lru_cache
def get_env(tdir=None):
    pard = cfg.get_config()
    pard.setdefault("JINJA_UNDEFINED", None)
    pard.setdefault("TEMPLATES_DIR", None)
    if tdir:
        pass
    elif pard["TEMPLATES_DIR"]:
        tdir = pard["TEMPLATES_DIR"]
    else:
        tdir = utils.sib_path(__name__, "template")
    tdir = tdir + "/"
    env = Environment(
        loader=FileSystemLoader(tdir),
        autoescape=select_autoescape(),
        trim_blocks=True,
        lstrip_blocks=True,
    )
    if pard["JINJA_UNDEFINED"] == "strict":
        env.undefined = StrictUndefined

    from . import url_for

    env.globals.update(
        {
            "APPSERVER": pard["APPSERVER"],
            "TITLE": pard["TITLE"],
            "LOGOUT_URL": pard["LOGOUT_URL"],
            "url_for": url_for,
        }
    )
    for k in filters:
        env.filters[k] = filters[k]

    return env


def add_filter(name, function, tdir=None):
    env = get_env(tdir)
    env.filters[name] = function


def render_template(template_name, **args):
    if "tdir" in args:
        tdir = args["tdir"]
    else:
        tdir = None
    env = get_env(tdir)
    template = env.get_template(template_name)
    return template.render(args)


def render_template_block(template_name, block_name, **args):
    if "tdir" in args:
        tdir = args["tdir"]
    else:
        tdir = None
    env = get_env(tdir)
    if isinstance(block_name, (list, tuple)):
        return render_blocks(env, template_name, block_name, **args)
    else:
        return render_block(env, template_name, block_name, **args)
