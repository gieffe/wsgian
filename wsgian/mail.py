"""
Use of "sendgrid" for email sending.

In your local configuration file :

'EMAIL': {
    'service': 'sendgrid',
    'default_sender': {'name': 'your project', 'email': 'youremail@yourdomain.com'},
    'api_url': 'https://api.sendgrid.com/v3/mail/send',
    'api_key': 'SENDGRID_API_KEY'}

for send email example see test_api_sendgrid()
------------------------------------------------------------------------------


Use gmail for send email

In your local configuration file :

'EMAIL': {
    'service': 'gmail',
    'default_sender': {'name': 'your project', 'email': 'youremail@yourdomain.com'},
    'oauth2_file': PATH_TO_OAUTH_JSON_CREDENTIALS_FILE
    }

example: see test_gmail()

FIRST RUN:
a link will be shown in the terminal that you should followed to obtain
a google_refresh_token. Paste this again, and you're set up

See docs at: https://github.com/kootenpv/yagmail

------------------------------------------------------------------------------

"""

import traceback
import smtplib
import ssl

# from email import Encoders
# from email.Message import Message
# from email.MIMEAudio import MIMEAudio
# from email.MIMEBase import MIMEBase
# from email.MIMEMultipart import MIMEMultipart
# from email.MIMEImage import MIMEImage
try:
    from email.MIMEText import MIMEText
except ModuleNotFoundError:
    from email.mime.text import MIMEText

import logging
import os


class EmailMessage:
    def __init__(self, sender, to, subject, body):
        self.sender = sender
        if isinstance(to, list):
            self.to = to
        else:
            self.to = [to]
        self.subject = subject
        self.body = body
        self.err = ""

    def send(self, pard={}):
        """Send email message

        Args:
                pard['SMTP_HOST'] default localhost
                pard['SMTP_PORT'] default ''
                pard['SMTP_USER'] default ''
                pard['SMTP_PASSW'] default ''
        """
        pard.setdefault("SMTP_HOST", "localhost")
        pard.setdefault("SMTP_PORT", 25)
        pard.setdefault("SMTP_USER", "")
        pard.setdefault("SMTP_PASSW", "")

        if pard["SMTP_HOST"] == "smtp.gmail.com":
            return self.gmail_send(pard)
        elif pard["SMTP_HOST"] == "":
            return self.mail_log()

        msg = MIMEText(self.body)
        msg["From"] = self.sender
        msg["Subject"] = self.subject
        msg["To"] = ", ".join(self.to)
        try:
            s = smtplib.SMTP(pard["SMTP_HOST"], pard["SMTP_PORT"])
            s.sendmail(self.sender, self.to, msg.as_string())
            s.close()
        except:
            self.err = traceback.format_exc()
            # raise
        self.mail_log()

    def gmail_send_old(self, pard={}):
        pard.setdefault("SMTP_HOST", "smtp.gmail.com")
        pard.setdefault("SMTP_PORT", 587)
        pard.setdefault("SMTP_USER", "")
        pard.setdefault("SMTP_PASSW", "")

        msg = MIMEText(self.body)
        msg["From"] = self.sender
        msg["Subject"] = self.subject
        msg["To"] = ", ".join(self.to)
        try:
            s = smtplib.SMTP(pard["SMTP_HOST"], pard["SMTP_PORT"])
            s.ehlo()
            s.starttls()
            s.ehlo()
            s.login(pard["SMTP_USER"], pard["SMTP_PASSW"])
            # s.set_debuglevel(1)
            s.sendmail(self.sender, self.to, msg.as_string())
            s.close()
        except:
            self.err = traceback.format_exc()
        self.mail_log()

    def gmail_send(self, pard={}):
        pard.setdefault("SMTP_HOST", "smtp.gmail.com")
        pard.setdefault("SMTP_PORT", 465)
        pard.setdefault("SMTP_USER", "")
        pard.setdefault("SMTP_PASSW", "")

        msg = MIMEText(self.body)
        msg["From"] = self.sender
        msg["Subject"] = self.subject
        msg["To"] = ", ".join(self.to)
        try:
            # Create a secure SSL context
            # context = ssl.create_default_context()
            s = smtplib.SMTP_SSL(pard["SMTP_HOST"], pard["SMTP_PORT"])
            s.login(pard["SMTP_USER"], pard["SMTP_PASSW"])
            # s.set_debuglevel(1)
            s.sendmail(self.sender, self.to, msg.as_string())
            s.close()
        except:
            self.err = traceback.format_exc()
        self.mail_log()

    def mail_log(self):
        logging.info("-" * 70)
        logging.info("Sender: %s" % self.sender)
        logging.info("To: %s" % self.to)
        logging.info("Subject: %s" % self.subject)
        logging.info("Body:\n%s" % self.body)
        if self.err:
            logging.error(self.err)


def test():
    """
    You can start a local SMTP debugging server by typing
    the following in Command Prompt:

    $ python -m smtpd -c DebuggingServer -n localhost:1025 [deprecated]

    or (require aiosmtpd library)

    $ python -m aiosmtpd -n -l localhost:1025

    Any emails sent through this server will be discarded and shown
    in the terminal window
    """
    logging.basicConfig(level=logging.DEBUG)
    message = EmailMessage(
        sender="Gianfranco Messori <messori.gf@gmail.com>",
        to=["gmessori@gmail.com"],
        subject="Prova wsgian email",
        body="Tutti i nodi vengono al pettine",
    )
    message.send({"SMTP_HOST": "localhost", "SMTP_PORT": 1025})


def test_gmail():
    logging.basicConfig(level=logging.DEBUG)
    pard = {"EMAIL": {"service": "gmail", "oauth2_file": "oauth2_creds.json"}}
    args = {
        "from": {"name": "Gianfranco Messori", "email": "messori.gf@gmail.com"},
        "to": [{"name": "Beppe", "email": "gmessori@gmail.com"}],
        "bcc": [{"name": "GF", "email": "messori.gf@gmail.com"}],
        "subject": "Prova wsgian email",
        "content": "Tutti i nodi vengono al pettine",
    }
    send(pard, args)


def test_api_smtp():
    logging.basicConfig(level=logging.DEBUG)
    pard = {"EMAIL": {"service": "SMTP", "smtp_host": "localhost", "smtp_port": 1025}}
    args = {
        "from": {"name": "Gianfranco Messori", "email": "messori.gf@gmail.com"},
        "to": [{"name": "Beppe", "email": "gmessori@gmail.com"}],
        "subject": "Prova wsgian email",
        "content": "Tutti i nodi vengono al pettine",
    }
    send(pard, args)


def test_api_sendgrid():
    logging.basicConfig(level=logging.DEBUG)
    pard = {
        "EMAIL": {
            "service": "sendgrid",
            "api_url": "https://api.sendgrid.com/v3/mail/send",
            "api_key": os.environ.get("SENDGRID_API_KEY"),
        }
    }
    args = {
        "from": {"name": "Kinder Supreme Team", "email": "messori.gf@gmail.com"},
        "to": [{"name": "Beppe", "email": "gmessori@gmail.com"}],
        "bcc": [{"name": "Gianfranco Messori", "email": "messori.gf@gmail.com"}],
        "subject": "Prova wsgian email",
        "content": "Tutti i nodi vengono al pettine\n O no?",
    }
    send(pard, args)


# ------------------------------------------------------------------- #
def send(pard, args):
    """
    New API Style methods for sending email

    Args:
            to, cc, bcc
            from,
            subject,
            content,
    """
    args.setdefault(
        "to",
        [
            {},
        ],
    )
    args.setdefault("from", {})
    args.setdefault("subject", "")
    args.setdefault("content", "")

    pard.setdefault("EMAIL", {"service": "SMTP"})
    if pard["EMAIL"]["service"] == "SMTP":
        result = smtp_send(pard, args)
    elif pard["EMAIL"]["service"] == "sendgrid":
        result = sendgrid(pard, args)
    elif pard["EMAIL"]["service"] == "gmail":
        result = gmail_send(pard, args)

    else:
        result = {
            "status": "Failure",
            "errors": "EMAIL Service `%(service)s` not available" % pard["EMAIL"],
        }

    mail_log(pard, args, result)
    return result


def smtp_send(pard, args):
    msg = MIMEText(args["content"])
    msg["From"] = "%(name)s <%(email)s>" % args["from"]
    msg["Subject"] = args["subject"]
    msg["To"] = ", ".join(["%(name)s <%(email)s>" % to for to in args["to"]])
    try:
        s = smtplib.SMTP(pard["EMAIL"]["smtp_host"], pard["EMAIL"]["smtp_port"])
        s.sendmail(msg["From"], msg["To"], msg.as_string())
        s.close()
        err = ""
    except:
        err = traceback.format_exc()
    result = {"status": "Success"}
    if err:
        result["status"] = "Failure"
        result["errors"] = err
    return result


def gmail_send(pard, args):
    import yagmail

    yargs = {}
    yargs["to"] = [to["email"] for to in args["to"]]
    if "cc" in args and args["cc"]:
        yargs["cc"] = [cc["email"] for cc in args["cc"]]
    if "bcc" in args and args["bcc"]:
        yargs["bcc"] = [bcc["email"] for bcc in args["bcc"]]
    yargs["subject"] = args["subject"]
    yargs["contents"] = args["content"]
    try:
        yag = yagmail.SMTP(
            args["from"]["email"], oauth2_file=pard["EMAIL"]["oauth2_file"]
        )
        yag.send(**yargs)
        err = ""
    except:
        err = traceback.format_exc()
    result = {"status": "Success"}
    if err:
        result["status"] = "Failure"
        result["errors"] = err
    return result


def sendgrid(pard, args):
    import requests
    from . import json

    api_url = pard["EMAIL"]["api_url"]
    api_key = pard["EMAIL"]["api_key"]

    persz = {"to": args["to"], "subject": args["subject"]}
    if "bcc" in args:
        persz["bcc"] = args["bcc"]
    if "cc" in args:
        persz["cc"] = args["cc"]

    payload = {
        "personalizations": [persz],
        "from": args["from"],
        "content": [{"type": "text/plain", "value": args["content"]}],
    }
    payload = json.encode(payload)
    logging.info(payload)

    headers = {
        "authorization": "Bearer %s" % api_key,
        "content-type": "application/json",
    }

    response = requests.post(api_url, data=payload, headers=headers)

    logging.info(response.status_code)
    return {"status": "Success", "message": response.content}


def mail_log(pard, args, result):
    logging.info("-" * 70)
    logging.info("Sender: %s" % str(args["from"]))
    logging.info("To: %s" % str(args["to"]))
    logging.info("Subject: %s" % args["subject"])
    logging.info("Body:\n%s" % str(args["content"]))
    if result["status"] == "Failure":
        logging.error("Send email failure:")
        logging.error(result["errors"])


# ------------------------------------------------------------------- #
# Server Name:  smtp.gmail.com
# Server port:  25 or 587
# Use Secure Sockets Layer (SSL):   Checked
# Authentication:   Password
if __name__ == "__main__":
    import sys

    try:
        action = sys.argv[1]
    except IndexError:
        action = ""
    if action == "test":
        test()
    elif action == "test_gmail":
        test_gmail()
    elif action == "test_api_smtp":
        test_api_smtp()
    elif action == "test_api_sendgrid":
        test_api_sendgrid()
    else:
        print(action)
