from . import mysql_api


def create_table(pard={}):
    print("CREATE TABLE IF NOT EXISTS `counters`")
    spam, sql = create_statement()
    mysql_api.execute(sql)


def create_statement():
    sql = """
        CREATE TABLE IF NOT EXISTS `counters` (
          `counter` int(10) unsigned NOT NULL,
          `name` varchar(255) NOT NULL,
          PRIMARY KEY (`name`)
        )
    """
    return ("counters", sql)


def get(pard={}, name=""):
    sql = """
        update counters set
            counter = last_insert_id(counter+1)
        where name = %(name)s
    """
    n = mysql_api.execute(sql, {"name": name})
    if not n:
        sql = """
            insert into counters
                (counter, name)
            values
                (1, %(name)s)
        """
        mysql_api.insert(sql, {"name": name})
        return 1
    else:
        sql = "select counter from counters where name = %(name)s"
        result = mysql_api.select_one(sql, {"name": name})
        return result["counter"]
