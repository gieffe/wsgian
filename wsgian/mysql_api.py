"""
Interface API for mysql Database

Authors: Alessandro Corradi, Gianfranco Messori

"""

import threading
import logging
import functools
import typing

import pymysql
from pymysql import cursors
from pymysql.constants import FIELD_TYPE, FLAG

from wsgian import cfg

logger = logging.getLogger(__name__)

pool = threading.local()


@functools.lru_cache
def get_config(alt_db=""):
    allowed_kwargs = {
        "database": str,
        "host": str,
        "port": int,
        "user": str,
        "password": str,
    }
    new_cnf = {}
    pard = cfg.get_config()
    sep = "." if alt_db else ""
    alt_db = "db.mysql" + sep + alt_db
    for kwarg, caster in allowed_kwargs.items():
        if kwarg in pard[alt_db]:
            new_cnf[kwarg] = caster(pard[alt_db][kwarg])
    new_cnf.setdefault("port", 3306)
    return new_cnf


def close_connections():
    conns = getattr(pool, "connections", {})
    for conn in conns.values():
        try:
            conn.close()

        except Exception:
            pass

    setattr(pool, "connections", {})


def connect(cnf=None, alt_db="", **conn_kwargs):
    if not hasattr(pool, "connections"):
        setattr(pool, "connections", {})

    if cnf is None:
        cnf = get_config(alt_db=alt_db)

    if alt_db in pool.connections:
        logger.debug("USING POOL CONNECTION TO: %s", alt_db or "default")
        conn = pool.connections[alt_db]

        if not getattr(conn, "open", True):
            logger.debug("RECONNECTING TO: %s", alt_db or "default")
            conn = real_connect(cnf, **conn_kwargs)

    else:
        logger.debug("CONNECTING TO: %s", alt_db or "default")
        conn = real_connect(cnf, **conn_kwargs)

    pool.connections[alt_db] = conn
    conn.converter = get_converter()
    return conn


@functools.lru_cache
def get_converter():
    conv = pymysql.converters.conversions.copy()
    str_or_binary = [(FLAG.BINARY, bytes), (None, str)]

    conv[FIELD_TYPE.STRING] = str_or_binary
    conv[FIELD_TYPE.VAR_STRING] = str_or_binary
    conv[FIELD_TYPE.VARCHAR] = str_or_binary
    conv[FIELD_TYPE.BLOB] = str_or_binary
    conv[FIELD_TYPE.JSON] = str
    return conv


# MySQL Error Alias
IntegrityError = pymysql.IntegrityError
OperationalError = pymysql.OperationalError
DatabaseError = pymysql.DatabaseError


def ping(alt_db: str = ""):
    pool.connections[alt_db].ping()


def real_connect(cnf, **kwargs):
    transform_dict = {
        "password": "passwd",
    }
    conn_values = {transform_dict.get(k, k): v for k, v in cnf.items()}
    conn_values.setdefault("autocommit", True)
    conn_values["charset"] = "utf8mb4"

    for k in ["conv"]:
        if k not in kwargs:
            continue
        conn_values[k] = kwargs[k]

    conn = pymysql.connect(**conn_values)
    conn.autocommit(kwargs.get("autocommit", True))

    with conn.cursor() as c:
        c.execute("set names utf8mb4")
    return conn


def select(query: str, params=None, alt_db="") -> tuple:
    """Returns a dict tuple for the given query and params."""

    conn = connect(None, alt_db=alt_db)

    with conn.cursor(cursors.DictCursor) as c:
        c.execute(query, params)
        results = c.fetchall()

    return results


def select_one(query, params=None, alt_db=""):
    """Exec ``query`` and return the first row"""

    conn = connect(None, alt_db=alt_db)

    with conn.cursor(cursors.DictCursor) as c:
        c.execute(query, params)
        result = c.fetchone()

    return result


def execute(query, params=None, alt_db=""):
    """
    Executes the given query, returning the number of updated rows
    or the lastrowid.
    """

    conn = connect(None, alt_db=alt_db)
    with conn.cursor(cursors.DictCursor) as c:
        c.execute(query, params)
        result = c.rowcount
    return result


def insert(query, params=None, alt_db=""):
    """Executes insert and return lastrowid, rowcount"""

    conn = connect(None, alt_db=alt_db)

    with conn.cursor(cursors.DictCursor) as c:
        c.execute(query, params)
        lastrowid = c.lastrowid
        rowcount = c.rowcount

    return lastrowid, rowcount


def bulk_insert(query, params, alt_db=""):
    """Executes the given query against all the given param sequences."""

    conn = connect(None, alt_db=alt_db)
    with conn.cursor(cursors.DictCursor) as c:
        c.executemany(query, params)
        rowcount = c.rowcount
    return rowcount


def mogrify(query, params=None, alt_db=""):
    """
    Returns the exact string that would be sent to the database by calling the
    execute() method.
    """

    conn = connect(None, alt_db=alt_db)
    with conn.cursor() as c:
        result = c.mogrify(query, params)
    return result


def escape(obj):
    #     if isinstance(obj, (int, float)):
    #         return str(obj)
    #     elif isinstance(obj, str):
    #         return pymysql.string_literal(obj.encode("utf-8")).decode("utf-8")
    #
    #     elif isinstance(obj, bytes):
    #         return pymysql.string_literal(obj)
    #
    #     return _escape_obj(obj)
    return pymysql.converters.escape_item(obj, "utf-8")


class _escape_obj:
    def __init__(self, obj):
        self._obj = obj

    def __getitem__(self, name):
        return escape(self._obj[name])

    def __getattr__(self, name):
        return escape(getattr(self._obj, name))


def escape_list(iterator: typing.Iterable) -> str:
    """Escape su un'elenco di elementi

    >>> escape_list([1, 2, 3, 4])
    '(1, 2, 3, 4)'
    """

    escaped_items = (escape(i) for i in iterator)
    escaped_str = ", ".join(escaped_items)

    if escaped_str == "":
        return "(select 1 where false)"

    return "(%s)" % escaped_str


# Alias
escape_string = escape
