__author__ = "Gianfranco Messori"

__all__ = ["ErrorList", "LoginError"]


class ErrorList(Exception):
    """Exception that looks like a list of errors

    Example:
        >>> try:
        ...     raise ErrorList(['pippo', 'pluto'])
        ... except ErrorList, err:
        ...     print err
        ...
        ['pippo', 'pluto']
        >>> err[0]
        'pippo'
        >>> err[1]
        'pluto'
        >>> for item in err: print item
        ...
        pippo
        pluto
        >>>
    """

    def __init__(self, value):
        if isinstance(value, str):
            self.value = [value]
        elif isinstance(value, list):
            self.value = value
        else:
            self.value = [str(value)]

    def __getitem__(self, i):
        return self.value[i]

    def __str__(self):
        return repr(self.value)


class LoginError(Exception):
    def __init__(self):
        self.value = "The email or password you entered is incorrect."

    def __str__(self):
        return self.value


# Update __all__ to contain all Property and Exception subclasses.
# for _name, _object in globals().items():
#   if ((_name.endswith('Property') and issubclass(_object, Property)) or
#       (_name.endswith('Error') and issubclass(_object, Exception))):
#     __all__.append(_name)
