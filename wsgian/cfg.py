"""
Utils to load global configuration of wsgian app
------------------------------------------------

1) Load configuration from a local python file
   (usually ignored by git or other cvs)

   - Create a site.config.py containig your global configuration
   config = {
     'HTTP_PROTOCOL': 'http',
     'HTTP_ADDRESS': 'localhost',
     'HTTP_PORT': 8080,
     ...
     }

    import site_config
    from wsgian import cfg
    pard = cfg.default_configuration()
    pard.update(site_config.config)


2) Load confinguration from environment or .env file

    from wsgian import cfg
    pard = cfg.default_configuration()
    pard = cfg.get_dot_env_config(pard, extra_params=[])

    Note: get_dot_env_config load only parameters alredy in the input
          dictionary or key in 'extra_params' list


3) Mix methods 1) and 2) toghether

    import site_config
    from wsgian import cfg
    pard = cfg.default_configuration()
    pard.update(site_config.config)
    pard = cfg.get_dot_env_config(pard)


4) Your own method

   Start from:

    from wsgian import cfg
    pard = cfg.default_configuration()

   Than add or override key to pard

    pard['PIPPO'] = 'Pluto'



"""

import os
from . import utils


def default_configuration():
    pard = {}

    ## App description
    pard["APPLICATION_NAME"] = "wsGianApp"
    pard["PROJECT"] = "wsgian"
    pard["PROJECT_TITLE"] = "Project Title"
    pard["PROJECT_SUBTITLE"] = "Project short description"
    pard["TITLE"] = "Project Title"

    ## HTTP
    pard["HTTP_PROTOCOL"] = "http"
    pard["HTTP_ADDRESS"] = "0.0.0.0"
    pard["HTTP_PORT"] = 80

    ## URL
    pard["APP_URL"] = ""
    pard["APPSERVER"] = "/"

    ## MySQL
    pard["MYSQL_USER"] = ""
    pard["MYSQL_PASSWORD"] = ""
    pard["MYSQL_HOST"] = ""
    pard["MYSQL_DB"] = ""
    #
    # another way to configure mysql
    pard["db.mysql"] = {}
    # pard['db.mysql'] =
    #        {'database': '',
    #         'host': '',
    #         'max_idle_time': 25200,
    #         'password': '',
    #         'user': ''},

    ## SQLite
    pard["SQLITE_DB"] = ""

    ## E-mail
    pard["EMAIL"] = {}

    #   Using sengrid
    #   'EMAIL': {
    #       'service': 'sendgrid',
    #       'default_sender': {'name': 'your project', 'email': 'youremail@yourdomain.com'},
    #       'api_url': 'https://api.sendgrid.com/v3/mail/send',
    #       'api_key': 'SENDGRID_API_KEY'}
    #
    #   Using local SMTP
    #   'EMAIL': {
    #       'service': 'SMTP',
    #       'default_sender': {'name': 'your project', 'email': 'youremail@yourdomain.com'},
    #       'smtp_host': 'localhost',
    #       'smtp_port': 1025,
    #       },
    #
    #   Using Gmail
    #   'EMAIL': {
    #       'service': 'gmail',
    #       'default_sender': {'name': 'your project', 'email': 'youremail@yourdomain.com'},
    #       'oauth2_file': PATH_TO_OAUTH_JSON_CREDENTIALS_FILE,
    #       },
    #

    # Parameters for backward compatibility only
    pard["SMTP_HOST"] = ""
    pard["SMTP_PORT"] = ""
    pard["SMTP_USER"] = ""
    pard["SMTP_PASSW"] = ""
    pard["DEFAULT_SENDER"] = ""
    # -------------------------------------------

    ## Google api integration
    pard["GOOGLE"] = {}

    # notes about goole login
    # -----------------------
    # pip install --upgrade google-api-python-client
    # pip install --upgrade google-auth google-auth-oauthlib google-auth-httplib2
    # pip install --upgrade requests
    #
    # see: https://developers.google.com/identity/sign-in/web/
    #
    # https://console.developers.google.com/
    #
    # add to site_config.py:
    #
    #  'GOOGLE': {u'auth_provider_x509_cert_url': u'https://www.googleapis.com/oauth2/v1/certs',
    #           u'auth_uri': u'https://accounts.google.com/o/oauth2/auth',
    #           u'client_id': u'webapp client id',
    #           u'client_secret': u'webapp client secret',
    #           u'javascript_origins': [list of javascript https origin urls],
    #           u'project_id': u'webapp project id',
    #           u'token_uri': u'https://oauth2.googleapis.com/token'}
    #

    ## Development/Production Server
    pard["TEMPLATES_DIR"] = None  # set to templates directory path
    pard["TEMPLATE_RELOAD"] = False
    pard["JINJA_UNDEFINED"] = None  # Set to "strict" to use StrictUndefined
    # as undefined class in jinja environment
    pard["with_static"] = True
    pard["STATIC_DIR"] = None  # if False, default will be used (../static)
    pard["with_ws"] = False
    pard["ws_auth"] = False
    pard["ws_version"] = 2
    pard["cookie_secret"] = "notapplicable"
    pard["DEBUG"] = False
    pard["LOGLEVEL"] = "debug"
    pard["APP_ERROR_LIST"] = []  # list of exceptions to be treated
    # as application errors
    pard["CGI_ESCAPE"] = True  # if true, all parameters arriving
    # from the form or url, will be processed
    # with cgi_escape before being passed
    # to the application except those that
    # contain the string _noesc_ in the name

    ## User authentication module (Note: gauth require mysql)
    pard["LANGUAGES"] = ["it", "en"]

    pard["AUTH_MODULE"] = None  # 'wsgian.gauth'
    pard["LOGIN_MODULE"] = "wsgian.gauth"
    pard["LOGIN_URL"] = "/login"
    pard["LOGOUT_URL"] = "/logout"
    pard["SIGNUP_URL"] = "/signup"

    return pard


def get_dot_env_config(pard, extra_params=[]):
    utils.dot_env()
    plist = list(pard.keys()) + extra_params
    for k in plist:
        if k in os.environ:
            pard[k] = os.environ[k]
    return pard


def menage_alternatives(pard):
    # called by wsgian App.__init__

    if "MYSQL_USER" in pard and pard["MYSQL_USER"]:
        pard["db.mysql"] = {
            "database": pard["MYSQL_DB"],
            "host": pard["MYSQL_HOST"],
            "user": pard["MYSQL_USER"],
            "password": pard["MYSQL_PASSWORD"],
            "max_idle_time": 25200,
        }

    if "SQLITE_DB" in pard and pard["SQLITE_DB"]:
        pard["db.sqlite"] = {"db_path": pard["SQLITE_DB"]}

    if "APP_URL" in pard and pard["APP_URL"]:
        pard["APPSERVER"] = pard["APP_URL"]
    else:
        pard["APPSERVER"] = "/"

    return pard


def get_config():
    """
    Utility method that get config parameters (.env method)
    """
    pard = default_configuration()
    pard = get_dot_env_config(pard, extra_params=[])
    pard = menage_alternatives(pard)
    return pard
